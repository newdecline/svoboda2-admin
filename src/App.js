import React, {useEffect} from 'react';
import {useDispatch} from "react-redux";
import {makeStyles} from "@material-ui/core/styles";
import Router from "./common/Router";
import routes from "./routes";
import {authorize} from "./store/actions/authorization";
import useHttpApi from "./hooks/useHttpApi";

const useStyles = makeStyles(() => ({
    app: {
        display: 'flex',
        flexDirection: 'column',
        height: '100vh',
    },
}));

const App = () => {
    const styles = useStyles();
    const dispatch = useDispatch();
    const {makeRequest} = useHttpApi();

    useEffect(() => {
        dispatch(authorize(makeRequest));
    }, []);

    return (
        <div className={styles.app}>
            <Router config={routes} />
        </div>
    );
};

export default App;