import React from "react";
import TabHelpPage from "../../../common/pages/TabHelpPage";

const SeoPage = () => {
    return (
        <TabHelpPage header="Инструменты для SEO оптимизации" />
    );
};

export default SeoPage;