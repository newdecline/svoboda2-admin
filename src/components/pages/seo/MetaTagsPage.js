import React from "react";
import Page from "../../../common/pages/Page";
import DataTable from "../../../common/tables/DataTable";
import {listPageMetas} from "../../../api/http/page-metas";
import PageMetaForm from "../../forms/PageMetaForm";

const MetaTagsPage = () => {
    return (
        <Page header="Мета теги">
            <DataTable
                columns={{
                    location: {},
                    title: {},
                }}
                fetchOptions={listPageMetas()}
                updateItemForm={PageMetaForm}
            />
        </Page>
    );
};

export default MetaTagsPage;