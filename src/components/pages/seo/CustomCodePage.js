import React from "react";
import Page from "../../../common/pages/Page";
import CustomCodeForm from "../../forms/CustomCodeForm";

const CustomCodePage = () => {
    return (
        <Page header="Сторонний код">
            <CustomCodeForm />
        </Page>
    );
};

export default CustomCodePage;