import React from "react";
import Page from "../../../common/pages/Page";
import SitemapForm from "../../forms/SitemapForm";

const SitemapPage = () => {
    return (
        <Page header="Редактирование sitemap.xml">
            <SitemapForm />
        </Page>
    );
};

export default SitemapPage;