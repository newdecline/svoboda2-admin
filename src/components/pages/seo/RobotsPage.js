import React from "react";
import Page from "../../../common/pages/Page";
import RobotsForm from "../../forms/RobotsForm";

const RobotsPage = () => {
    return (
        <Page header="Редактирование robots.txt">
            <RobotsForm />
        </Page>
    );
};

export default RobotsPage;