import React from "react";
import Page from "../../../common/pages/Page";
import DataTable from "../../../common/tables/DataTable";
import {listFeedbackForms} from "../../../api/http/feedback-forms";
import FeedbackForm from "../../forms/FeedbackForm";

const FeedbackFormsPage = () => {
    return (
        <Page header="Формы обратной связи">
            <DataTable
                polymorphic
                columns={{
                    location: {},
                }}
                fetchOptions={listFeedbackForms()}
                updateItemForm={FeedbackForm}
            />
        </Page>
    );
};

export default FeedbackFormsPage;