import React from "react";
import Page from "../../../common/pages/Page";
import DataTable from "../../../common/tables/DataTable";
import {listGalleries} from "../../../api/http/galleries";
import GalleryForm from "../../forms/GalleryForm";

const GalleriesPage = () => {
    return (
        <Page header="Галереи">
            <DataTable
                columns={{
                    location: {},
                }}
                fetchOptions={listGalleries()}
                updateItemForm={GalleryForm}
            />
        </Page>
    );
};

export default GalleriesPage;