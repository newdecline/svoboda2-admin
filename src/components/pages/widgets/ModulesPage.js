import React from "react";
import TabHelpPage from "../../../common/pages/TabHelpPage";

const ModulesPage = () => {
    return (
        <TabHelpPage header="Все модули в одном месте" />
    );
};

export default ModulesPage;