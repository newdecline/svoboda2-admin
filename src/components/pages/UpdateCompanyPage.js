import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/index";
import useQuery from "../../hooks/useQuery";
import useHttpApi from "../../hooks/useHttpApi";
import {deleteCompany} from "../../api/http/companies";
import Page from "../../common/pages/Page";
import UpdateCompanyForm from "../forms/UpdateCompanyForm";
import GalleryForm from "../forms/GalleryForm";
import PageMetaForm from "../forms/PageMetaForm";
import Divider from "@material-ui/core/Divider";
import Button from "@material-ui/core/Button";
import ConfirmModal from "../../common/modals/ConfirmModal";
import {useHistory} from "react-router-dom";

const AFTER_DELETE_ROUTE = '/content/companies';

const useStyles = makeStyles(theme => ({
    buttons: {
        display: 'flex',
        marginTop: theme.spacing(3),
    },
}));

const UpdateCompanyPage = () => {
    const styles = useStyles();
    const {makeRequest} = useHttpApi();
    const id = useQuery().get('id');
    const history = useHistory();

    const [pageMetaId, setPageMetaId] = useState(null);
    const [gallery, setGallery] = useState(null);
    const [deleteModalIsOpen, setDeleteModalIsOpen] = useState(false);

    const handleDelete = async () => {
        const response = await makeRequest(deleteCompany(id));
        response.status === 204 && history.push(AFTER_DELETE_ROUTE);
    };

    return (
        <Page header="Редактирование жителя" backButton>
            <UpdateCompanyForm
                id={id}
                afterPrefetch={company => {
                    setPageMetaId(company.pageMeta.id);
                    setGallery(company.gallery);
                }}
            />
            <GalleryForm id={gallery && gallery.id} type={gallery && gallery.type} />
            <PageMetaForm id={pageMetaId} />
            <Divider />
            <div className={styles.buttons}>
                <Button
                    variant="contained"
                    color="secondary"
                    onClick={() => setDeleteModalIsOpen(true)}
                >
                    Удалить жителя
                </Button>
            </div>
            <ConfirmModal
                message="Вы уверены, что хотите удалить жителя?"
                confirmButtonText="Удалить"
                confirmButtonColor="secondary"
                isOpen={deleteModalIsOpen}
                onCancel={() => setDeleteModalIsOpen(false)}
                onConfirm={handleDelete}
            />
        </Page>
    );
};

export default UpdateCompanyPage;