import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/index";
import useHttpApi from "../../hooks/useHttpApi";
import useQuery from "../../hooks/useQuery";
import {useHistory} from "react-router-dom";
import {deleteArea} from "../../api/http/areas";
import Page from "../../common/pages/Page";
import GalleryForm from "../forms/GalleryForm";
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import ConfirmModal from "../../common/modals/ConfirmModal";
import UpdateAreaForm from "../forms/UpdateAreaForm";

const AFTER_DELETE_ROUTE = '/content/home-page/areas';

const useStyles = makeStyles(theme => ({
    buttons: {
        display: 'flex',
        marginTop: theme.spacing(3),
    },
}));

const UpdateAreaPage = () => {
    const styles = useStyles();
    const {makeRequest} = useHttpApi();
    const id = useQuery().get('id');
    const history = useHistory();

    const [gallery, setGallery] = useState(null);
    const [deleteModalIsOpen, setDeleteModalIsOpen] = useState(false);

    const handleDelete = async () => {
        const response = await makeRequest(deleteArea(id));
        response.status === 204 && history.push(AFTER_DELETE_ROUTE);
    };

    return (
        <Page header="Редактирование площадки" backButton>
            <UpdateAreaForm
                id={id}
                afterPrefetch={area => setGallery(area.gallery)}
            />
            <GalleryForm id={gallery && gallery.id} type={gallery && gallery.type} />
            <Divider />
            <div className={styles.buttons}>
                <Button
                    variant="contained"
                    color="secondary"
                    onClick={() => setDeleteModalIsOpen(true)}
                >
                    Удалить площадку
                </Button>
            </div>
            <ConfirmModal
                message="Вы уверены, что хотите удалить площадку?"
                confirmButtonText="Удалить"
                confirmButtonColor="secondary"
                isOpen={deleteModalIsOpen}
                onCancel={() => setDeleteModalIsOpen(false)}
                onConfirm={handleDelete}
            />
        </Page>
    );
};

export default UpdateAreaPage;