import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import {useSelector} from "react-redux";
import {getProfile} from "../../store/reducers/rootReducer";
import moment from "moment";
import {Grid, Typography, Fade, Box} from "@material-ui/core";

const useStyles = makeStyles(() => ({
    grid: {
        height: '100%',
    },
}));

const greetings = [
    {
        range: [0, 5],
        message: 'Доброй ночи',
    },
    {
        range: [5, 12],
        message: 'С добрым утром',
    },
    {
        range: [12, 17],
        message: 'Добрый день',
    },
    {
        range: [17, 24],
        message: 'Добрый вечер',
    }
];

const hour = moment().hour();
const greeting = greetings.find(item => item.range[0] <= hour && hour < item.range[1]).message;

const DashboardPage = () => {
    const styles = useStyles();
    const {name} = useSelector(getProfile);

    return (
        <Grid
            container
            justify="center"
            alignItems="center"
            className={styles.grid}
        >
            <Fade in timeout={1000}>
                <Box>
                    <Typography variant="h2" gutterBottom={true}>
                        {`${greeting}, ${name}!`}
                    </Typography>
                    <Typography variant="subtitle1" color="textSecondary" align="center">
                        Вы можете выбрать категорию администрирования выше
                    </Typography>
                </Box>
            </Fade>
        </Grid>
    );
};

export default DashboardPage;