import React from "react";
import Page from "../../../../common/pages/Page";
import OwnEventSectionForm from "../../../forms/OwnEventSectionForm";

const OwnEventSectionPage = () => {
    return (
        <Page header='Секция "Проведи своё мероприятие"' headerLink="/#provedi-meropriyatie">
            <OwnEventSectionForm />
        </Page>
    );
};

export default OwnEventSectionPage;