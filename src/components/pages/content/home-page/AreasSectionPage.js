import React from "react";
import Page from "../../../../common/pages/Page";
import AreasTable from "../../../tables/AreasTable";
import FeedbackForm from "../../../forms/FeedbackForm";
import {useSelector} from "react-redux";
import {getHomePage} from "../../../../store/reducers/rootReducer";

const AreasSectionPage = () => {
    const {areaFormIndex} = useSelector(getHomePage);

    return (
        <Page header="Площадки" headerLink="/#provedi-meropriyatie">
            <AreasTable />
            <FeedbackForm index={areaFormIndex} />
        </Page>
    );
};

export default AreasSectionPage;