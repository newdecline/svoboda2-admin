import React from "react";
import Page from "../../../../common/pages/Page";
import EventsSectionForm from "../../../forms/EventsSectionForm";
import EventsTable from "../../../tables/EventsTable";

const EventsSectionPage = () => {
    return (
        <Page header='Секция "Афиша"' headerLink="/#afisha">
            <EventsSectionForm />
            <EventsTable />
        </Page>
    );
};

export default EventsSectionPage;