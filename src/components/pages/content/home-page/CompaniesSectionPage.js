import React from "react";
import Page from "../../../../common/pages/Page";
import CompaniesSectionForm from "../../../forms/CompaniesSectionForm";

const CompaniesSectionPage = () => {
    return (
        <Page header='Секция "Жители"' headerLink="/#zhiteli">
            <CompaniesSectionForm />
        </Page>
    );
};

export default CompaniesSectionPage;