import React from "react";
import Page from "../../../../common/pages/Page";
import RoomsTable from "../../../tables/RoomsTable";
import RentSectionForm from "../../../forms/RentSectionForm";

const RentSectionPage = () => {
    return (
        <Page header='Секция "Аренда"' headerLink="/#arenda">
            <RentSectionForm />
            <RoomsTable />
        </Page>
    );
};

export default RentSectionPage;