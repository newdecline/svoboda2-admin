import React from "react";
import Page from "../../../../common/pages/Page";
import {useSelector} from "react-redux";
import {getHomePage} from "../../../../store/reducers/rootReducer";
import GalleryForm from "../../../forms/GalleryForm";
import AboutSectionForm from "../../../forms/AboutSectionForm";

const AboutSectionPage = () => {
    const {aboutGallery} = useSelector(getHomePage);

    return (
        <Page header="Секция о компании" headerLink="/#svoboda2">
            <AboutSectionForm />
            <GalleryForm id={aboutGallery.id} type={aboutGallery.type} />
        </Page>
    );
};

export default AboutSectionPage;