import React from "react";
import Page from "../../../../common/pages/Page";
import {useSelector} from "react-redux";
import {getHomePage} from "../../../../store/reducers/rootReducer";
import ContactsSectionForm from "../../../forms/ContactsSectionForm";
import FeedbackForm from "../../../forms/FeedbackForm";

const ContactsSectionPage = () => {
    const {feedbackFormIndex} = useSelector(getHomePage);

    return (
        <Page header="Секция с контактами" headerLink="/#kontakty">
            <ContactsSectionForm />
            <FeedbackForm index={feedbackFormIndex} />
        </Page>
    );
};

export default ContactsSectionPage;