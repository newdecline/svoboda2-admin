import React from "react";
import {useSelector} from "react-redux";
import {getHomePage} from "../../../../store/reducers/rootReducer";
import Page from "../../../../common/pages/Page";
import TopSectionForm from "../../../forms/TopSectionForm";
import GalleryForm from "../../../forms/GalleryForm";

const TopSectionPage = () => {
    const {mainBanner} = useSelector(getHomePage);

    return (
        <Page header="Верхняя секция" headerLink="/#glavnaya">
            <GalleryForm id={mainBanner.id} type={mainBanner.type} />
            <TopSectionForm />
        </Page>
    );
};

export default TopSectionPage;