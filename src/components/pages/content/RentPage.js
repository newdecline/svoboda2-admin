import React from "react";
import {useSelector} from "react-redux";
import {getRentPage} from "../../../store/reducers/rootReducer";
import Page from "../../../common/pages/Page";
import PageMetaForm from "../../forms/PageMetaForm";
import RentPageForm from "../../forms/RentPageForm";
import RoomsTable from "../../tables/RoomsTable";

const RentPage = () => {
    const {pageMetaId} = useSelector(getRentPage);

    return (
        <Page header='Страница "Аренда"' headerLink="/arenda">
            <RentPageForm />
            <RoomsTable />
            <PageMetaForm id={pageMetaId} />
        </Page>
    );
};

export default RentPage;