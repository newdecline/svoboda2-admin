import React from "react";
import Page from "../../../common/pages/Page";
import {useSelector} from "react-redux";
import PageMetaForm from "../../forms/PageMetaForm";
import {getHomePage} from "../../../store/reducers/rootReducer";

const HomePage = () => {
    const {pageMetaId} = useSelector(getHomePage);

    return (
        <Page header="Главная страница" headerLink="/">
            <PageMetaForm id={pageMetaId} />
        </Page>
    );
};

export default HomePage;