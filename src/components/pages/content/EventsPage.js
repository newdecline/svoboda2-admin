import React from "react";
import Page from "../../../common/pages/Page";
import {useSelector} from "react-redux";
import {getEventsPage} from "../../../store/reducers/rootReducer";
import PageMetaForm from "../../forms/PageMetaForm";
import EventsTable from "../../tables/EventsTable";
import EventsPageForm from "../../forms/EventsPageForm";

const EventsPage = () => {
    const {pageMetaId} = useSelector(getEventsPage);

    return (
        <Page header='Страница "Афиша"' headerLink="/afisha">
            <EventsPageForm />
            <EventsTable />
            <PageMetaForm id={pageMetaId} />
        </Page>
    );
};

export default EventsPage;