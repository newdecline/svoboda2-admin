import React from "react";
import {useSelector} from "react-redux";
import {getCompaniesPage} from "../../../store/reducers/rootReducer";
import Page from "../../../common/pages/Page";
import PageMetaForm from "../../forms/PageMetaForm";
import CompaniesPageForm from "../../forms/CompaniesPageForm";
import CompanyTagsTable from "../../tables/CompanyTagsTable";
import CompaniesTable from "../../tables/CompaniesTable";

const CompaniesPage = () => {
    const {pageMetaId} = useSelector(getCompaniesPage);

    return (
        <Page header='Страница "Жители"' headerLink="/zhiteli">
            <CompaniesPageForm />
            <CompanyTagsTable />
            <CompaniesTable />
            <PageMetaForm id={pageMetaId} />
        </Page>
    );
};

export default CompaniesPage;