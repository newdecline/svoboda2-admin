import React from "react";
import Page from "../../../common/pages/Page";
import LayoutForm from "../../forms/LayoutForm";

const LayoutPage = () => {
    return (
        <Page
            header="Сквозные элементы"
            headerHint="Расположены в шапке или в подвале сайта"
            headerLink="/"
        >
            <LayoutForm />
        </Page>
    );
};

export default LayoutPage;