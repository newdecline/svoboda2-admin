import React, {useState} from "react";
import useQuery from "../../hooks/useQuery";
import UpdateEventForm from "../forms/UpdateEventForm";
import Page from "../../common/pages/Page";
import PageMetaForm from "../forms/PageMetaForm";
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import {makeStyles} from "@material-ui/core/index";
import ConfirmModal from "../../common/modals/ConfirmModal";
import useHttpApi from "../../hooks/useHttpApi";
import {deleteEvent} from "../../api/http/events";
import {useHistory} from "react-router-dom";
import GalleryForm from "../forms/GalleryForm";

const AFTER_DELETE_ROUTE = '/content/events';

const useStyles = makeStyles(theme => ({
    buttons: {
        display: 'flex',
        marginTop: theme.spacing(3),
    },
}));

const UpdateEventPage = () => {
    const styles = useStyles();
    const {makeRequest} = useHttpApi();
    const id = useQuery().get('id');
    const history = useHistory();

    const [pageMetaId, setPageMetaId] = useState(null);
    const [gallery, setGallery] = useState(null);
    const [deleteModalIsOpen, setDeleteModalIsOpen] = useState(false);

    const handleDelete = async () => {
        const response = await makeRequest(deleteEvent(id));
        response.status === 204 && history.push(AFTER_DELETE_ROUTE);
    };

    return (
        <Page header="Редактирование мероприятия" backButton>
            <UpdateEventForm
                id={id}
                afterPrefetch={event => {
                    setPageMetaId(event.pageMeta.id);
                    setGallery(event.gallery);
                }}
            />
            <GalleryForm id={gallery && gallery.id} type={gallery && gallery.type} />
            <PageMetaForm id={pageMetaId} />
            <Divider />
            <div className={styles.buttons}>
                <Button
                    variant="contained"
                    color="secondary"
                    onClick={() => setDeleteModalIsOpen(true)}
                >
                    Удалить мероприятие
                </Button>
            </div>
            <ConfirmModal
                message="Вы уверены, что хотите удалить мероприятие?"
                confirmButtonText="Удалить"
                confirmButtonColor="secondary"
                isOpen={deleteModalIsOpen}
                onCancel={() => setDeleteModalIsOpen(false)}
                onConfirm={handleDelete}
            />
        </Page>
    );
};

export default UpdateEventPage;