import React from "react";
import Page from "../../common/pages/Page";
import ProfileForm from "../forms/ProfileForm";

const ProfilePage = () => {
    return (
        <Page header="Профиль">
            <ProfileForm />
        </Page>
    );
};

export default ProfilePage;