import React from "react";
import Page from "../../../common/pages/Page";
import DataTable from "../../../common/tables/DataTable";
import {listUsers} from "../../../api/http/users";

const UsersPage = () => {
    return (
        <Page
            header="Пользователи"
            headerHint="Пользователи, имеющие доступ к панели администрирования"
        >
            <DataTable
                columns={{
                    email: {},
                    name: {},
                }}
                fetchOptions={listUsers()}
            />
        </Page>
    );
};

export default UsersPage;