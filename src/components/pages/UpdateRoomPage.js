import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/index";
import useQuery from "../../hooks/useQuery";
import useHttpApi from "../../hooks/useHttpApi";
import {useHistory} from "react-router-dom";
import {deleteRoom} from "../../api/http/rooms";
import Page from "../../common/pages/Page";
import GalleryForm from "../forms/GalleryForm";
import PageMetaForm from "../forms/PageMetaForm";
import Divider from "@material-ui/core/Divider";
import Button from "@material-ui/core/Button";
import ConfirmModal from "../../common/modals/ConfirmModal";
import UpdateRoomForm from "../forms/UpdateRoomForm";

const AFTER_DELETE_ROUTE = '/content/rent';

const useStyles = makeStyles(theme => ({
    buttons: {
        display: 'flex',
        marginTop: theme.spacing(3),
    },
}));

const UpdateRoomPage = () => {
    const styles = useStyles();
    const {makeRequest} = useHttpApi();
    const id = useQuery().get('id');
    const history = useHistory();

    const [pageMetaId, setPageMetaId] = useState(null);
    const [gallery, setGallery] = useState(null);
    const [deleteModalIsOpen, setDeleteModalIsOpen] = useState(false);

    const handleDelete = async () => {
        const response = await makeRequest(deleteRoom(id));
        response.status === 204 && history.push(AFTER_DELETE_ROUTE);
    };

    return (
        <Page header="Редактирование помещения" backButton>
            <UpdateRoomForm
                id={id}
                afterPrefetch={room => {
                    setPageMetaId(room.pageMeta.id);
                    setGallery(room.gallery);
                }}
            />
            <GalleryForm id={gallery && gallery.id} type={gallery && gallery.type} />
            <PageMetaForm id={pageMetaId} />
            <Divider />
            <div className={styles.buttons}>
                <Button
                    variant="contained"
                    color="secondary"
                    onClick={() => setDeleteModalIsOpen(true)}
                >
                    Удалить помещение
                </Button>
            </div>
            <ConfirmModal
                message="Вы уверены, что хотите удалить помещение?"
                confirmButtonText="Удалить"
                confirmButtonColor="secondary"
                isOpen={deleteModalIsOpen}
                onCancel={() => setDeleteModalIsOpen(false)}
                onConfirm={handleDelete}
            />
        </Page>
    );
};

export default UpdateRoomPage;