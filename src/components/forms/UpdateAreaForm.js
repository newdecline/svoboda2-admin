import React from "react";
import PrefetchingForm from "../../common/forms/PrefetchingForm";
import {getArea, updateArea} from "../../api/http/areas";

const UpdateAreaForm = props => {
    return (
        <PrefetchingForm
            {...props}
            loading={!props.id}
            fields={{
                name: {},
                bullet_description: {
                    options: {
                        multiline: true,
                    },
                },
                bullet_capacity: {
                    options: {
                        multiline: true,
                    },
                },
                bullet_rent: {
                    options: {
                        multiline: true,
                    },
                },
                bullet_contact: {
                    options: {
                        multiline: true,
                    },
                },
                is_hidden: {
                    type: 'checkbox',
                    label: 'Опубликовано',
                    options: {
                        invertValue: true,
                    },
                },
            }}
            prefetchOptions={getArea(props.id)}
            submitOptions={data => updateArea(props.id, data)}
        />
    );
};

export default UpdateAreaForm;