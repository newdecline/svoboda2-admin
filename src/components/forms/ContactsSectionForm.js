import React from "react";
import PrefetchingForm from "../../common/forms/PrefetchingForm";
import {getHomePage, updateHomePage} from "../../api/http/home-page";

const ContactsSectionForm = props => {
    return (
        <PrefetchingForm
            {...props}
            fields={{
                contacts_section_header: {},
                contacts_address: {},
                contacts_phone_1: {},
                contacts_phone_2: {},
                contacts_email: {},
                contacts_coordinates: {
                    hint: "Координаты адреса можно получить, например, с помощью геокодера Яндекса",
                },
            }}
            layout={[
                ['contacts_section_header'],
                ['contacts_address'],
                ['contacts_phone_1', 'contacts_phone_2'],
                ['contacts_email'],
                ['contacts_coordinates'],
            ]}
            prefetchOptions={getHomePage()}
            usePrefetchCaching
            cacheExtrasOnly
            prefetchCacheKey={'contactsSectionForm'}
            submitOptions={data => updateHomePage(data)}
        />
    );
};

export default ContactsSectionForm;