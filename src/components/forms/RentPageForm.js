import React from "react";
import PrefetchingForm from "../../common/forms/PrefetchingForm";
import {getRentPage, updateRentPage} from "../../api/http/rent-page";

const RentPageForm = props => {
    const isFirstStepHasError = stepsHasError => stepsHasError[0];
    const isCroppingStepDisabled = (stepsHasError, {values}) =>
        isFirstStepHasError(stepsHasError) || values.bannerImageOriginalFileId === null;

    return (
        <PrefetchingForm
            {...props}
            header="Баннер"
            fields={{
                bannerImageOriginalFileId: {
                    type: 'file',
                    options: {
                        initialImage: 'bannerImageOriginal',
                    }
                },
                bannerImageFileId: {
                    type: 'cropper',
                    options: {
                        origin: 'bannerImageOriginalFileId',
                        initialImage: 'bannerImage',
                    },
                },
                bannerImageAlt: {},
            }}
            layout={[
                {
                    type: 'stepper',
                    options: {
                        orientation: 'vertical',
                        steps: [
                            {
                                label: 'Загрузите исходный файл',
                                subLabel: 'Исходный файл используется только для кадрирования и не выводится на сайте',
                                layout: [
                                    ['bannerImageOriginalFileId'],
                                ],
                            },
                            {
                                label: 'Кадрируйте изображение',
                                subLabel: 'Кадрируйте изображение с учетом допустимых пропорций',
                                layout: [
                                    ['bannerImageAlt'],
                                    ['bannerImageFileId'],
                                ],
                                disabled: isCroppingStepDisabled,
                            },
                        ],
                    },
                },
            ]}
            prefetchOptions={getRentPage()}
            usePrefetchCaching
            cacheExtrasOnly
            prefetchCacheKey={'rentPageForm'}
            submitOptions={data => updateRentPage(data)}
        />
    );
};

export default RentPageForm;