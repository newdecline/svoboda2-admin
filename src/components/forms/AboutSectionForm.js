import React from "react";
import PrefetchingForm from "../../common/forms/PrefetchingForm";
import {getHomePage, updateHomePage} from "../../api/http/home-page";

const AboutSectionForm = props => {
    return (
        <PrefetchingForm
            {...props}
            fields={{
                show_about_section: {
                    type: 'checkbox',
                },
                about_section_header: {},
                about_text: {
                    options: {
                        multiline: true,
                    },
                },
            }}
            prefetchOptions={getHomePage()}
            usePrefetchCaching
            cacheExtrasOnly
            prefetchCacheKey={'aboutSectionForm'}
            submitOptions={data => updateHomePage(data)}
        />
    );
};

export default AboutSectionForm;