import React from "react";
import PrefetchingForm from "../../common/forms/PrefetchingForm";
import {getCustomCode, updateCustomCode} from "../../api/http/customCode";

const CustomCodeForm = () => {
    return (
        <PrefetchingForm
            fields={{
                head: {
                    type: 'code',
                    options: {
                        name: 'headCodeAceEditor',
                        mode: 'html',
                    },
                },
                footer: {
                    type: 'code',
                    options: {
                        name: 'footerCodeAceEditor',
                        mode: 'html',
                    },
                },
            }}
            prefetchOptions={getCustomCode()}
            usePrefetchCaching
            cacheExtrasOnly
            prefetchCacheKey={'customCodeForm'}
            submitOptions={data => updateCustomCode(data)}
        />
    );
};

export default CustomCodeForm;