import React from "react";
import {createEvent, getCreateEventForm} from "../../api/http/events";
import EventForm from "./EventForm";

const CreateEventForm = props => {
    return (
        <EventForm
            {...props}
            header="Добавление мероприятия"
            layout={[
                ['name'],
                ['archive_date', null],
                ['date', 'use_archive_date_as_date'],
            ]}
            prefetchOptions={getCreateEventForm()}
            submitOptions={data => createEvent(data)}
        />
    );
};

export default CreateEventForm;