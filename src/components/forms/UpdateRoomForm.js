import React from "react";
import RoomForm from "./RoomForm";
import {getRoom, updateRoom} from "../../api/http/rooms";

const onChangeMiddleware = {
    is_hidden: (value, updateField) =>
        value && updateField('show_on_home_page', false),
};

const fieldMiddleware = {
    show_on_home_page: (config, {is_hidden}) => ({
        ...config,
        options: {
            ...config.options,
            disabled: is_hidden,
        },
    }),
};

const UpdateRoomForm = props => {
    const isFirstStepHasError = stepsHasError => stepsHasError[0];
    const isCroppingStepDisabled = (stepsHasError, {values}) =>
        isFirstStepHasError(stepsHasError) || values.originalImageFileId === null;

    return (
        <RoomForm
            {...props}
            loading={!props.id}
            fields={{
                description: {
                    options: {
                        multiline: true,
                    },
                },
                is_hidden: {
                    type: 'checkbox',
                    label: 'Опубликовано',
                    options: {
                        invertValue: true,
                    },
                },
                show_on_home_page: {
                    type: 'checkbox',
                },
                originalImageFileId: {
                    type: 'file',
                    options: {
                        initialImage: 'originalImage',
                    },
                },
                bannerImageFileId: {
                    type: 'cropper',
                    options: {
                        origin: 'originalImageFileId',
                        initialImage: 'bannerImage',
                    }
                },
                thumbnailImageFileId: {
                    type: 'cropper',
                    options: {
                        origin: 'originalImageFileId',
                        initialImage: 'thumbnailImage',
                    }
                },
                bannerImageAlt: {},
                thumbnailImageAlt: {},
            }}
            layout={[
                ['name'],
                ['location'],
                ['square'],
                ['cost'],
                ['phone'],
                {
                    type: 'stepper',
                    options: {
                        header: 'Изображения',
                        orientation: 'vertical',
                        steps: [
                            {
                                label: 'Загрузите исходный файл',
                                subLabel: 'Исходный файл используется только для кадрирования и не выводится на сайте',
                                layout: [
                                    ['originalImageFileId'],
                                ],
                            },
                            {
                                label: 'Кадрируйте изображение под баннер',
                                subLabel: 'Это изображение будет показываться в баннере на странице помещения',
                                layout: [
                                    ['bannerImageAlt'],
                                    ['bannerImageFileId'],
                                ],
                                disabled: isCroppingStepDisabled,
                            },
                            {
                                label: 'Кадрируйте изображение под карточку',
                                subLabel: 'Это изображение будет показываться в списках помещений',
                                layout: [
                                    ['thumbnailImageAlt'],
                                    ['thumbnailImageFileId'],
                                ],
                                disabled: isCroppingStepDisabled,
                            },
                        ],
                    },
                },
                ['description'],
                ['is_hidden', 'show_on_home_page'],
            ]}
            prefetchOptions={getRoom(props.id)}
            submitOptions={data => updateRoom(props.id, data)}
            onChangeMiddleware={onChangeMiddleware}
            fieldMiddleware={fieldMiddleware}
        />
    );
};

export default UpdateRoomForm;