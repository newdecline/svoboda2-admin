import React from "react";
import {getHomePage, updateHomePage} from "../../api/http/home-page";
import PrefetchingForm from "../../common/forms/PrefetchingForm";

const OwnEventSectionForm = props => {
    const isFirstStepHasError = stepsHasError => stepsHasError[0];
    const isCroppingStepDisabled = (stepsHasError, {values}) =>
        isFirstStepHasError(stepsHasError) || values.ownEventOriginalImageFileId === null;

    return (
        <PrefetchingForm
            {...props}
            fields={{
                show_own_event_section: {
                    type: 'checkbox',
                },
                own_event_section_header: {},
                own_event_text: {
                    options: {
                        multiline: true,
                    },
                },
                ownEventOriginalImageFileId: {
                    type: 'file',
                    options: {
                        initialImage: 'ownEventOriginalImage',
                    },
                },
                ownEventImageFileId: {
                    type: 'cropper',
                    options: {
                        origin: 'ownEventOriginalImageFileId',
                        initialImage: 'ownEventImage',
                    }
                },
                ownEventImageAlt: {},
            }}
            layout={[
                ['show_own_event_section'],
                ['own_event_section_header'],
                ['own_event_text'],
                {
                    type: 'stepper',
                    options: {
                        header: 'Изображение в правой части секции',
                        orientation: 'vertical',
                        steps: [
                            {
                                label: 'Загрузите исходный файл',
                                subLabel: 'Исходный файл используется только для кадрирования и не выводится на сайте',
                                layout: [
                                    ['ownEventOriginalImageFileId'],
                                ],
                            },
                            {
                                label: 'Кадрируйте изображение',
                                subLabel: 'Кадрируйте изображение с учетом допустимых пропорций',
                                layout: [
                                    ['ownEventImageAlt'],
                                    ['ownEventImageFileId'],
                                ],
                                disabled: isCroppingStepDisabled,
                            },
                        ],
                    },
                },
            ]}
            prefetchOptions={getHomePage()}
            usePrefetchCaching
            cacheExtrasOnly
            prefetchCacheKey={'ownEventSectionForm'}
            submitOptions={data => updateHomePage(data)}
        />
    );
};

export default OwnEventSectionForm;