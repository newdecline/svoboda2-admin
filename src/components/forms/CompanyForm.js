import React from "react";
import PrefetchingForm from "../../common/forms/PrefetchingForm";

const CompanyForm = props => {
    const commonFields = {
        list_name: {
            hint: 'Выводится общем списке жителей',
        },
        name: {
            options: {
                multiline: true,
            },
            hint: 'Выводится на странице жителя',
        },
        location: {
            hint: 'Расположение на SVOBODA2',
        },
        address: {
            hint: 'Полный адрес. Выводится на странице жителя в секции "Контакты"',
        },
    };

    return (
        <PrefetchingForm
            {...props}
            fields={{
                ...commonFields,
                ...props.fields,
            }}
        />
    );
};

export default CompanyForm;