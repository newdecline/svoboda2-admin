import React from "react";
import PrefetchingForm from "../../common/forms/PrefetchingForm";
import {getHomePage, updateHomePage} from "../../api/http/home-page";

const TopSectionForm = props => {
    return (
        <PrefetchingForm
            {...props}
            header={"Предложения"}
            fields={{
                offer_1: {
                    options: {
                        multiline: true,
                    },
                },
                offer_2: {
                    options: {
                        multiline: true,
                    },
                },
                offer_3: {
                    options: {
                        multiline: true,
                    },
                },
            }}
            prefetchOptions={getHomePage()}
            usePrefetchCaching
            cacheExtrasOnly
            prefetchCacheKey={'topSectionForm'}
            submitOptions={data => updateHomePage(data)}
        />
    );
};

export default TopSectionForm;