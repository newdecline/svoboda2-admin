import React from "react";
import PolymorphicForm from "../../common/forms/PolymorphicForm";
import {getFeedbackForm, updateFeedbackForm} from "../../api/http/feedback-forms";
import {get} from "lodash";

const FeedbackForm = props => {
    return (
        <PolymorphicForm
            {...props}
            loading={!props.index}
            type={get(props, 'index.model_type')}
            fields={{
                email: {
                    hint: "На этот адрес будет отправлено письмо с заявкой"
                },
            }}
            polymorphic={{
                feedback_form: {
                    header: "Форма обратной связи",
                    fields: {
                        text: {
                            options: {
                                multiline: true,
                            },
                        },
                    },
                },
                area_form: {
                    header: "Форма бронирования площадки",
                    fields: {
                        text: {
                            options: {
                                multiline: true,
                            },
                        },
                    },
                },
            }}
            prefetchOptions={getFeedbackForm(props.index.id)}
            submitOptions={data => updateFeedbackForm(props.index.id, data)}
        />
    );
};

export default FeedbackForm;