import React from "react";
import PrefetchingForm from "../../common/forms/PrefetchingForm";
import {getPageMeta, updatePageMeta} from "../../api/http/page-metas";

const PageMetaForm = props => {
    return (
        <PrefetchingForm
            {...props}
            header="Мета теги страницы"
            loading={!props.id}
            fields={{
                title: {},
                description: {
                    options: {
                        multiline: true,
                    },
                },
                keywords: {},
            }}
            prefetchOptions={getPageMeta(props.id)}
            submitOptions={data => updatePageMeta(props.id, data)}
        />
    );
};

export default PageMetaForm;