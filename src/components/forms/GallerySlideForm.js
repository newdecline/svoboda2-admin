import React from "react";
import PolymorphicForm from "../../common/forms/PolymorphicForm";
import {TYPE_BANNER, TYPE_GALLERY} from "./GalleryForm";

const GallerySlideForm = props => {
    const isFirstStepHasError = stepsHasError => stepsHasError[0];
    const isCroppingStepDisabled = (stepsHasError, {values}) =>
        isFirstStepHasError(stepsHasError) || values.fileToCropId === null;

    const commonFields = {
        originalImageAlt: {},
        thumbnailImageAlt: {},
        text: {},
    };

    return (
        <PolymorphicForm
            {...props}
            fields={{
                ...commonFields,
                ...props.fields,
            }}
            polymorphic={{
                [TYPE_BANNER]: {
                    ...props.polymorphic[TYPE_BANNER],
                    layout: [
                        {
                            type: 'stepper',
                            options: {
                                orientation: 'vertical',
                                steps: [
                                    {
                                        label: 'Загрузите исходный файл',
                                        subLabel: 'Исходный файл используется только для кадрирования и не выводится на сайте',
                                        layout: [
                                            ['fileToCropId'],
                                        ],
                                    },
                                    {
                                        label: 'Кадрируйте изображение',
                                        subLabel: 'Кадрируйте изображение с учетом допустимых пропорций',
                                        layout: [
                                            ['originalImageAlt'],
                                            ['originalFileId'],
                                        ],
                                        disabled: isCroppingStepDisabled,
                                    },
                                    {
                                        label: 'Заполните остальные поля',
                                        layout: [
                                            ['text'],
                                        ],
                                    },
                                ],
                            },
                        },
                    ],
                },
                [TYPE_GALLERY]: {
                    ...props.polymorphic[TYPE_GALLERY],
                    layout: [
                        {
                            type: 'stepper',
                            options: {
                                orientation: 'vertical',
                                steps: [
                                    {
                                        label: 'Загрузите исходный файл',
                                        subLabel: 'Исходный файл используется только для кадрирования и не выводится на сайте',
                                        layout: [
                                            ['fileToCropId'],
                                        ],
                                    },
                                    {
                                        label: 'Кадрируйте изображение под полный размер',
                                        subLabel: 'Это изображение будет открываться при клике по миниатюре',
                                        layout: [
                                            ['originalImageAlt'],
                                            ['originalFileId'],
                                        ],
                                        disabled: isCroppingStepDisabled,
                                    },
                                    {
                                        label: 'Кадрируйте изображение под миниатюру',
                                        layout: [
                                            ['thumbnailImageAlt'],
                                            ['thumbnailFileId'],
                                        ],
                                        disabled: isCroppingStepDisabled,
                                    },
                                    {
                                        label: 'Заполните остальные поля',
                                        layout: [
                                            ['text'],
                                        ],
                                    },
                                ],
                            },
                        },
                    ],
                },
            }}
        />
    );
};

export default GallerySlideForm;