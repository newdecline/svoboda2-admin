import React from "react";
import PrefetchingForm from "../../common/forms/PrefetchingForm";
import {getProfile, updateProfile} from "../../api/http/profile";
import {useDispatch} from "react-redux";
import {updateProfileName} from "../../store/actions/dashboard";

const ProfileForm = () => {
    const dispatch = useDispatch();

    return (
        <PrefetchingForm
            fields={{
                // avatarImage: {
                //     type: 'image',
                // },
                name: {},
            }}
            prefetchOptions={getProfile()}
            usePrefetchCaching
            cacheExtrasOnly
            prefetchCacheKey={'profileForm'}
            submitOptions={data => updateProfile(data)}
            onSuccess={({name}) => dispatch(updateProfileName(name))}
        />
    );
};

export default ProfileForm;