import React from "react";
import PrefetchingForm from "../../common/forms/PrefetchingForm";

const CompanyTagForm = props => {
    return (
        <PrefetchingForm
            {...props}
            fields={{
                name: {},
            }}
        />
    );
};

export default CompanyTagForm;