import React from "react";
import {getImage, updateImage} from "../../api/http/images";
import PrefetchingForm from "../../common/forms/PrefetchingForm";

const UpdateImageForm = props => {
    return (
        <PrefetchingForm
            {...props}
            header="Редактирование изображения"
            fields={{
                fileId: {
                    type: 'file',
                },
                alt: {},
            }}
            prefetchOptions={getImage(props.id)}
            submitOptions={data => updateImage(props.id, data)}
            isModal
        />
    );
};

export default UpdateImageForm;