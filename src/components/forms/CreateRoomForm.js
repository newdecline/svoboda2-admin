import React from "react";
import RoomForm from "./RoomForm";
import {createRoom, getCreateRoomForm} from "../../api/http/rooms";

const CreateRoomForm = props => {
    return (
        <RoomForm
            {...props}
            header="Добавление помещения"
            prefetchOptions={getCreateRoomForm()}
            submitOptions={data => createRoom(data)}
        />
    );
};

export default CreateRoomForm;