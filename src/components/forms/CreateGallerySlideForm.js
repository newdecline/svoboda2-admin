import React from "react";
import {createGallerySlide, getCreateGallerySlideForm} from "../../api/http/galleries";
import GallerySlideForm from "./GallerySlideForm";
import {TYPE_BANNER, TYPE_GALLERY} from "./GalleryForm";



const CreateGallerySlideForm = props => {
    return (
        <GallerySlideForm
            {...props}
            fields={{
                fileToCropId: {
                    type: 'file',
                },
                originalFileId: {
                    type: 'cropper',
                    options: {
                        origin: 'fileToCropId',
                    }
                },
            }}
            polymorphic={{
                [TYPE_BANNER]: {
                    header: 'Добавление баннера',
                },
                [TYPE_GALLERY]: {
                    header: 'Добавление изображения',
                    fields: {
                        thumbnailFileId: {
                            type: 'cropper',
                            options: {
                                origin: 'fileToCropId',
                            }
                        },
                    },
                },
            }}
            prefetchOptions={getCreateGallerySlideForm(props.id)}
            submitOptions={data => createGallerySlide(props.id, data)}
        />
    );
};

export default CreateGallerySlideForm;