import React from "react";
import PrefetchingForm from "../../common/forms/PrefetchingForm";
import moment from "moment/moment";

const dateFormat = "D MMMM H:mm";
const archiveDateFormat = 'DD.MM.YYYY HH:mm';

const convertArchiveDateToDate = (archiveDate, archiveDateFormat) => {
    const dateMoment = moment(archiveDate, archiveDateFormat, true);

    return dateMoment.isValid() ? dateMoment.format(dateFormat) : '';
};

const onChangeMiddleware = {
    // TODO Так не работает, потому что {format} не передается при изменении чекбокса.
    // use_archive_date_as_date: (value, updateField, {archive_date}, {format}) =>
    //     value && updateField('date', convertArchiveDateToDate(archive_date, format)),
    //
    // archive_date: (value, updateField, {use_archive_date_as_date}, {format}) =>
    //     use_archive_date_as_date && updateField('date', convertArchiveDateToDate(value, format)),

    use_archive_date_as_date: (value, updateField, {archive_date}) =>
        value && updateField('date', convertArchiveDateToDate(archive_date, archiveDateFormat)),

    archive_date: (value, updateField, {use_archive_date_as_date}) =>
        use_archive_date_as_date && updateField('date', convertArchiveDateToDate(value, archiveDateFormat)),
};

const fieldMiddleware = {
    date: (config, {use_archive_date_as_date}) => ({
        ...config,
        options: {
            ...config.options,
            disabled: use_archive_date_as_date,
        },
    }),
};

const EventForm = props => {
    const commonFields = {
        name: {
            options: {
                multiline: true,
            },
        },
        archive_date: {
            type: 'dateTime',
        },
        use_archive_date_as_date: {
            type: 'checkbox',
        },
        date: {},
    };

    return (
        <PrefetchingForm
            {...props}
            fields={{
                ...commonFields,
                ...props.fields,
            }}
            onChangeMiddleware={{...onChangeMiddleware, ...props.onChangeMiddleware}}
            fieldMiddleware={{...fieldMiddleware, ...props.fieldMiddleware}}
        />
    );
};

export default EventForm;