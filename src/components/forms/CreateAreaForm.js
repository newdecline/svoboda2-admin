import React from "react";
import PrefetchingForm from "../../common/forms/PrefetchingForm";
import {createArea, getCreateAreaForm} from "../../api/http/areas";

const CreateAreaForm = props => {
    return (
        <PrefetchingForm
            {...props}
            header="Добавление площадки"
            fields={{
                name: {},
            }}
            prefetchOptions={getCreateAreaForm()}
            submitOptions={data => createArea(data)}
        />
    );
};

export default CreateAreaForm;