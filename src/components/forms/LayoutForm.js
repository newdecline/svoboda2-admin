import React from "react";
import PrefetchingForm from "../../common/forms/PrefetchingForm";
import {getLayout, updateLayout} from "../../api/http/layout";

const LayoutForm = () => {
    return (
        <PrefetchingForm
            fields={{
                instagram_url: {},
                vk_url: {},
                city: {},
                phone: {},
                address: {},
                email: {},
                copyright: {},
            }}
            layout={[
                ['instagram_url', 'vk_url'],
                ['phone', 'email'],
                ['city', 'address'],
                ['copyright'],
            ]}
            prefetchOptions={getLayout()}
            usePrefetchCaching
            cacheExtrasOnly
            prefetchCacheKey={'layoutForm'}
            submitOptions={data => updateLayout(data)}
        />
    );
};

export default LayoutForm;