import React from "react";
import PrefetchingForm from "../../common/forms/PrefetchingForm";
import {getCompaniesPage, updateCompaniesPage} from "../../api/http/companies-page";

const CompaniesPageForm = props => {
    const isFirstStepHasError = stepsHasError => stepsHasError[0];
    const isCroppingStepDisabled = (stepsHasError, {values}) =>
        isFirstStepHasError(stepsHasError) || values.bannerImageOriginalFileId === null;

    return (
        <PrefetchingForm
            {...props}
            header="Баннер"
            fields={{
                bannerImageOriginalFileId: {
                    type: 'file',
                    options: {
                        initialImage: 'bannerImageOriginal',
                    }
                },
                bannerImageFileId: {
                    type: 'cropper',
                    options: {
                        origin: 'bannerImageOriginalFileId',
                        initialImage: 'bannerImage',
                    },
                },
                bannerImageAlt: {},
            }}
            layout={[
                {
                    type: 'stepper',
                    options: {
                        orientation: 'vertical',
                        steps: [
                            {
                                label: 'Загрузите исходный файл',
                                subLabel: 'Исходный файл используется только для кадрирования и не выводится на сайте',
                                layout: [
                                    ['bannerImageOriginalFileId'],
                                ],
                            },
                            {
                                label: 'Кадрируйте изображение',
                                subLabel: 'Кадрируйте изображение с учетом допустимых пропорций',
                                layout: [
                                    ['bannerImageAlt'],
                                    ['bannerImageFileId'],
                                ],
                                disabled: isCroppingStepDisabled,
                            },
                        ],
                    },
                },
            ]}
            prefetchOptions={getCompaniesPage()}
            usePrefetchCaching
            cacheExtrasOnly
            prefetchCacheKey={'companiesPageForm'}
            submitOptions={data => updateCompaniesPage(data)}
        />
    );
};

export default CompaniesPageForm;