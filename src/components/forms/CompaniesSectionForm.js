import React from "react";
import PrefetchingForm from "../../common/forms/PrefetchingForm";
import {getHomePage, updateHomePage} from "../../api/http/home-page";

const CompaniesSectionForm = props => {
    const isFirstStepHasError = stepsHasError => stepsHasError[0];
    const isCroppingStepDisabled = (stepsHasError, {values}) =>
        isFirstStepHasError(stepsHasError) || values.companiesOriginalImageFileId === null;

    return (
        <PrefetchingForm
            {...props}
            fields={{
                show_companies_section: {
                    type: 'checkbox',
                },
                companies_section_header: {},
                companies_text: {
                    options: {
                        multiline: true,
                    },
                },
                companiesOriginalImageFileId: {
                    type: 'file',
                    options: {
                        initialImage: 'companiesOriginalImage',
                    },
                },
                companiesImageFileId: {
                    type: 'cropper',
                    options: {
                        origin: 'companiesOriginalImageFileId',
                        initialImage: 'companiesImage',
                    }
                },
                companiesImageAlt: {},
            }}
            layout={[
                ['show_companies_section'],
                ['companies_section_header'],
                ['companies_text'],
                {
                    type: 'stepper',
                    options: {
                        header: 'Изображение в правой части секции',
                        orientation: 'vertical',
                        steps: [
                            {
                                label: 'Загрузите исходный файл',
                                subLabel: 'Исходный файл используется только для кадрирования и не выводится на сайте',
                                layout: [
                                    ['companiesOriginalImageFileId'],
                                ],
                            },
                            {
                                label: 'Кадрируйте изображение',
                                subLabel: 'Кадрируйте изображение с учетом допустимых пропорций',
                                layout: [
                                    ['companiesImageAlt'],
                                    ['companiesImageFileId'],
                                ],
                                disabled: isCroppingStepDisabled,
                            },
                        ],
                    },
                },
            ]}
            prefetchOptions={getHomePage()}
            usePrefetchCaching
            cacheExtrasOnly
            prefetchCacheKey={'companiesSectionForm'}
            submitOptions={data => updateHomePage(data)}
        />
    );
};

export default CompaniesSectionForm;