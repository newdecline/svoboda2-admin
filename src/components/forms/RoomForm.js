import React from "react";
import PrefetchingForm from "../../common/forms/PrefetchingForm";

const RoomForm = props => {
    const commonFields = {
        name: {
            options: {
                multiline: true,
            },
        },
        location: {},
        square: {},
        cost: {},
        phone: {},
    };

    return (
        <PrefetchingForm
            {...props}
            fields={{
                ...commonFields,
                ...props.fields,
            }}
        />
    );
};

export default RoomForm;