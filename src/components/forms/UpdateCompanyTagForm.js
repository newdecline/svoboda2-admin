import React from "react";
import CompanyTagForm from "./CompanyTagForm";
import {getCompanyTag, updateCompanyTag} from "../../api/http/company-tag";

const UpdateCompanyTagForm = props => {
    return (
        <CompanyTagForm
            {...props}
            loading={!props.id}
            header="Редактирование вида деятельности"
            prefetchOptions={getCompanyTag(props.id)}
            submitOptions={data => updateCompanyTag(props.id, data)}
        />
    );
};

export default UpdateCompanyTagForm;