import React from "react";
import CompanyForm from "./CompanyForm";
import {createCompany, getCreateCompanyForm} from "../../api/http/companies";

const CreateCompanyForm = props => {
    return (
        <CompanyForm
            {...props}
            header="Добавление жителя"
            prefetchOptions={getCreateCompanyForm()}
            submitOptions={data => createCompany(data)}
        />
    );
};

export default CreateCompanyForm;