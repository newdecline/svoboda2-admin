import React from "react";
import {getEvent, updateEvent} from "../../api/http/events";
import EventForm from "./EventForm";

const onChangeMiddleware = {
    is_hidden: (value, updateField) =>
        value && updateField('show_on_home_page', false),
};

const fieldMiddleware = {
    show_on_home_page: (config, {is_hidden}) => ({
        ...config,
        options: {
            ...config.options,
            disabled: is_hidden,
        },
    }),
};

const UpdateEventForm = props => {
    const isFirstStepHasError = stepsHasError => stepsHasError[0];
    const isCroppingStepDisabled = (stepsHasError, {values}) =>
        isFirstStepHasError(stepsHasError) || values.originalImageFileId === null;

    return (
        <EventForm
            {...props}
            loading={!props.id}
            fields={{
                description_link: {},
                description_link_text: {},
                description_place: {},
                description_price: {},
                description_contact: {},
                description_text: {
                    options: {
                        multiline: true,
                    },
                },
                is_hidden: {
                    type: 'checkbox',
                    label: 'Опубликовано',
                    options: {
                        invertValue: true,
                    },
                },
                show_on_home_page: {
                    type: 'checkbox',
                },
                originalImageFileId: {
                    type: 'file',
                    options: {
                        initialImage: 'originalImage',
                    },
                },
                bannerImageFileId: {
                    type: 'cropper',
                    options: {
                        origin: 'originalImageFileId',
                        initialImage: 'bannerImage',
                    }
                },
                thumbnailImageFileId: {
                    type: 'cropper',
                    options: {
                        origin: 'originalImageFileId',
                        initialImage: 'thumbnailImage',
                    }
                },
                bannerImageAlt: {},
                thumbnailImageAlt: {},
            }}
            layout={[
                ['name'],
                ['archive_date', null],
                ['date', 'use_archive_date_as_date'],
                {
                    type: 'stepper',
                    options: {
                        header: 'Изображения',
                        orientation: 'vertical',
                        steps: [
                            {
                                label: 'Загрузите исходный файл',
                                subLabel: 'Исходный файл используется только для кадрирования и не выводится на сайте',
                                layout: [
                                    ['originalImageFileId'],
                                ],
                            },
                            {
                                label: 'Кадрируйте изображение под баннер',
                                subLabel: 'Это изображение будет показываться в баннере на странице мероприятия',
                                layout: [
                                    ['bannerImageAlt'],
                                    ['bannerImageFileId'],
                                ],
                                disabled: isCroppingStepDisabled,
                            },
                            {
                                label: 'Кадрируйте изображение под карточку',
                                subLabel: 'Это изображение будет показываться в списках мероприятий',
                                layout: [
                                    ['thumbnailImageAlt'],
                                    ['thumbnailImageFileId'],
                                ],
                                disabled: isCroppingStepDisabled,
                            },
                        ],
                    },
                },
                ['description_link', 'description_link_text'],
                ['description_place'],
                ['description_price'],
                ['description_contact'],
                ['description_text'],
                ['is_hidden', 'show_on_home_page'],
            ]}
            prefetchOptions={getEvent(props.id)}
            submitOptions={data => updateEvent(props.id, data)}
            onChangeMiddleware={onChangeMiddleware}
            fieldMiddleware={fieldMiddleware}
        />
    );
};

export default UpdateEventForm;