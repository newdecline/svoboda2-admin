import React from "react";
import PrefetchingForm from "../../common/forms/PrefetchingForm";
import {getEventsPage, updateEventsPage} from "../../api/http/events-page";

const EventsPageForm = props => {
    const isFirstStepHasError = stepsHasError => stepsHasError[0];
    const isCroppingStepDisabled = (stepsHasError, {values}) =>
        isFirstStepHasError(stepsHasError) || values.bannerImageOriginalFileId === null;

    return (
        <PrefetchingForm
            {...props}
            header="Баннер"
            fields={{
                banner_header: {
                    options: {
                        multiline: true,
                    },
                },
                banner_sub_header: {},
                banner_link: {},
                bannerImageOriginalFileId: {
                    type: 'file',
                    options: {
                        initialImage: 'bannerImageOriginal',
                    }
                },
                bannerImageFileId: {
                    type: 'cropper',
                    options: {
                        origin: 'bannerImageOriginalFileId',
                        initialImage: 'bannerImage',
                    },
                },
                bannerImageAlt: {},
            }}
            layout={[
                {
                    type: 'stepper',
                    options: {
                        orientation: 'vertical',
                        steps: [
                            {
                                label: 'Загрузите исходный файл',
                                subLabel: 'Исходный файл используется только для кадрирования и не выводится на сайте',
                                layout: [
                                    ['bannerImageOriginalFileId'],
                                ],
                            },
                            {
                                label: 'Кадрируйте изображение',
                                subLabel: 'Кадрируйте изображение с учетом допустимых пропорций',
                                layout: [
                                    ['bannerImageAlt'],
                                    ['bannerImageFileId'],
                                ],
                                disabled: isCroppingStepDisabled,
                            },
                            {
                                label: 'Заполните остальные поля',
                                layout: [
                                    ['banner_header'],
                                    ['banner_sub_header'],
                                    ['banner_link'],
                                ],
                            },
                        ],
                    },
                },
            ]}
            prefetchOptions={getEventsPage()}
            usePrefetchCaching
            cacheExtrasOnly
            prefetchCacheKey={'eventsPageForm'}
            submitOptions={data => updateEventsPage(data)}
        />
    );
};

export default EventsPageForm;