import React from "react";
import PrefetchingForm from "../../common/forms/PrefetchingForm";
import {getHomePage, updateHomePage} from "../../api/http/home-page";

const EventsSectionForm = props => {
    return (
        <PrefetchingForm
            {...props}
            fields={{
                show_events_section: {
                    type: 'checkbox',
                },
                events_section_header: {},
                events_text: {
                    options: {
                        multiline: true,
                    },
                },
            }}
            prefetchOptions={getHomePage()}
            usePrefetchCaching
            cacheExtrasOnly
            prefetchCacheKey={'eventsSectionForm'}
            submitOptions={data => updateHomePage(data)}
        />
    );
};

export default EventsSectionForm;