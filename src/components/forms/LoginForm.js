import React from "react";
import PrefetchingForm from "../../common/forms/PrefetchingForm";
import {getLoginForm, login as loginRequest} from "../../api/http/authorization";
import {useDispatch} from "react-redux";
import useHttpApi from "../../hooks/useHttpApi";
import {login} from "../../store/actions/authorization";

const LoginForm = () => {
    const {makeRequest} = useHttpApi();
    const dispatch = useDispatch();

    return (
        <PrefetchingForm
            header="Войти"
            buttonText="Войти"
            buttonProps={{fullWidth: true}}
            fields={{
                email: {},
                password: {
                    options: {
                        type: 'password',
                    },
                },
                rememberMe: {
                    type: 'checkbox',
                },
            }}
            prefetchOptions={getLoginForm()}
            usePrefetchCaching
            prefetchCacheKey={'loginForm'}
            submitOptions={data => loginRequest(data)}
            onSuccess={({value: token}) => dispatch(login(token, makeRequest))}
        />
    );
};

export default LoginForm;