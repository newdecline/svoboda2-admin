import React from "react";
import CompanyTagForm from "./CompanyTagForm";
import {createCompanyTag, getCreateCompanyTagForm} from "../../api/http/company-tag";

const CreateCompanyTagForm = props => {
    return (
        <CompanyTagForm
            {...props}
            header="Добавление вида деятельности"
            prefetchOptions={getCreateCompanyTagForm()}
            submitOptions={data => createCompanyTag(data)}
        />
    );
};

export default CreateCompanyTagForm;