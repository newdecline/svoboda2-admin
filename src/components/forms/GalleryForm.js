import React from "react";
import ImagesDataGrid from "../../common/grids/ImagesDataGrid";
import {
    deleteGallerySlide,
    listGallerySlides,
    moveNextGallerySlide,
    movePrevGallerySlide,
    updateGallerySlideVisibility
} from "../../api/http/galleries";
import CreateGallerySlideForm from "./CreateGallerySlideForm";
import UpdateGallerySlideForm from "./UpdateGallerySlideForm";

export const TYPE_BANNER = 'banner';
export const TYPE_GALLERY = 'gallery';

const DEFAULT_HEADERS = {
    [TYPE_BANNER]: 'Баннеры',
    [TYPE_GALLERY]: 'Галерея',
};

const GalleryForm = props => {
    const {
        type,
        id,
        header,
    } = props;

    return (
        <ImagesDataGrid
            {...props}
            formProps={{type}}
            header={header || DEFAULT_HEADERS[type]}
            forceLoading={!Boolean(id)}
            fetchOptions={listGallerySlides(id)}
            itemImage={'originalImage'}
            itemIsHidden={'is_hidden'}
            createItemForm={CreateGallerySlideForm}
            updateItemForm={UpdateGallerySlideForm}
            moveRightItemOptions={({id}) => moveNextGallerySlide(id)}
            moveLeftItemOptions={({id}) => movePrevGallerySlide(id)}
            updateItemVisibilityOptions={({id}, isHidden) => updateGallerySlideVisibility(id, isHidden)}
            deleteItemOptions={({id}) => deleteGallerySlide(id)}
        />
    );
};

export default GalleryForm;