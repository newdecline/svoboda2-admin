import React from "react";
import PrefetchingForm from "../../common/forms/PrefetchingForm";
import {getRobots, updateRobots} from "../../api/http/static-files";

const RobotsForm = () => {
    return (
        <PrefetchingForm
            fields={{
                content: {
                    type: 'code',
                    options: {
                        name: 'robotsTxtAceEditor',
                        // TODO Почему xml?
                        mode: 'xml',
                    },
                },
            }}
            prefetchOptions={getRobots()}
            usePrefetchCaching
            cacheExtrasOnly
            prefetchCacheKey={'robotsForm'}
            submitOptions={data => updateRobots(data)}
        />
    );
};

export default RobotsForm;