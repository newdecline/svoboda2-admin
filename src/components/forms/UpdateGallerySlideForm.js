import React from "react";
import {getGallerySlide, updateGallerySlide} from "../../api/http/galleries";
import GallerySlideForm from "./GallerySlideForm";
import {TYPE_BANNER, TYPE_GALLERY} from "./GalleryForm";

const UpdateGallerySlideForm = props => {
    return (
        <GallerySlideForm
            {...props}
            header="Редактирование слайда"
            fields={{
                fileToCropId: {
                    type: 'file',
                    options: {
                        initialImage: 'imageToCrop',
                    },
                },
                originalFileId: {
                    type: 'cropper',
                    options: {
                        origin: 'fileToCropId',
                        initialImage: 'originalImage',
                    }
                },
            }}
            polymorphic={{
                [TYPE_BANNER]: {
                    header: 'Редактирование баннера',
                },
                [TYPE_GALLERY]: {
                    header: 'Редактирование изображения',
                    fields: {
                        thumbnailFileId: {
                            type: 'cropper',
                            options: {
                                origin: 'fileToCropId',
                                initialImage: 'thumbnailImage',
                            }
                        },
                    },
                },
            }}
            prefetchOptions={getGallerySlide(props.id)}
            submitOptions={data => updateGallerySlide(props.id, data)}
        />
    );
};

export default UpdateGallerySlideForm;