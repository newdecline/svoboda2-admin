import React from "react";
import CompanyForm from "./CompanyForm";
import {getCompany, updateCompany} from "../../api/http/companies";
import {listAllowedToSet} from "../../api/http/company-tag";

const UpdateCompanyForm = props => {
    const isFirstStepHasError = stepsHasError => stepsHasError[0];
    const isAvatarCroppingStepDisabled = (stepsHasError, {values}) =>
        isFirstStepHasError(stepsHasError) || values.originalAvatarFileId === null;
    const isBannerCroppingStepDisabled = (stepsHasError, {values}) =>
        isFirstStepHasError(stepsHasError) || values.originalBannerFileId === null;

    return (
        <CompanyForm
            {...props}
            loading={!props.id}
            fields={{
                is_hidden: {
                    type: 'checkbox',
                    label: 'Опубликовано',
                    options: {
                        invertValue: true,
                    },
                },
                vk_url: {},
                instagram_url: {},
                facebook_url: {},
                site_url: {},
                description: {
                    options: {
                        multiline: true,
                    },
                },
                phone: {},
                email: {},
                renter_from: {
                    hint: 'Например, "с сентября 2019"',
                },

                originalAvatarFileId: {
                    type: 'file',
                    options: {
                        initialImage: 'originalAvatarImage',
                    },
                },
                avatarFileId: {
                    type: 'cropper',
                    options: {
                        origin: 'originalAvatarFileId',
                        initialImage: 'avatarImage',
                    },
                },
                avatarAlt: {},

                originalBannerFileId: {
                    type: 'file',
                    options: {
                        initialImage: 'originalBannerImage',
                    },
                },
                bannerFileId: {
                    type: 'cropper',
                    options: {
                        origin: 'originalBannerFileId',
                        initialImage: 'bannerImage',
                    },
                },
                bannerAlt: {},

                locationImageFileId: {
                    type: 'file',
                    options: {
                        initialImage: 'locationImage',
                    },
                },
                locationImageAlt: {},

                tagIds: {
                    type: 'select',
                    options: {
                        multiple: true,
                        fetchOptions: listAllowedToSet(),
                    },
                },
            }}
            layout={[
                ['list_name', 'name'],
                ['location', 'address'],

                ['tagIds'],

                {
                    type: 'stepper',
                    options: {
                        header: 'Логотип',
                        orientation: 'vertical',
                        steps: [
                            {
                                label: 'Загрузите исходный файл',
                                subLabel: 'Исходный файл используется только для кадрирования и не выводится на сайте',
                                layout: [
                                    ['originalAvatarFileId'],
                                ],
                            },
                            {
                                label: 'Кадрируйте изображение',
                                subLabel: 'Логотип выводится на странице "Жители"',
                                layout: [
                                    ['avatarAlt'],
                                    ['avatarFileId'],
                                ],
                                disabled: isAvatarCroppingStepDisabled,
                            },
                        ],
                    },
                },
                {
                    type: 'stepper',
                    options: {
                        header: 'Баннер',
                        orientation: 'vertical',
                        steps: [
                            {
                                label: 'Загрузите исходный файл',
                                subLabel: 'Исходный файл используется только для кадрирования и не выводится на сайте',
                                layout: [
                                    ['originalBannerFileId'],
                                ],
                            },
                            {
                                label: 'Кадрируйте изображение под баннер',
                                subLabel: 'Это изображение будет показываться в баннере на странице жителя',
                                layout: [
                                    ['bannerAlt'],
                                    ['bannerFileId'],
                                ],
                                disabled: isBannerCroppingStepDisabled,
                            },
                        ],
                    },
                },
                ['locationImageAlt'],
                ['locationImageFileId'],

                ['vk_url'],
                ['instagram_url'],
                ['facebook_url'],
                ['site_url'],

                ['phone'],
                ['email'],
                ['renter_from'],

                ['description'],

                ['is_hidden'],
            ]}
            prefetchOptions={getCompany(props.id)}
            submitOptions={data => updateCompany(props.id, data)}
        />
    );
};

export default UpdateCompanyForm;