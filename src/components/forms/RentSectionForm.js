import React from "react";
import PrefetchingForm from "../../common/forms/PrefetchingForm";
import {getHomePage, updateHomePage} from "../../api/http/home-page";

const RentSectionForm = props => {
    return (
        <PrefetchingForm
            {...props}
            fields={{
                show_rent_section: {
                    type: 'checkbox',
                },
                rent_section_header: {},
                rent_text: {
                    options: {
                        multiline: true,
                    },
                },
            }}
            prefetchOptions={getHomePage()}
            usePrefetchCaching
            cacheExtrasOnly
            prefetchCacheKey={'rentSectionForm'}
            submitOptions={data => updateHomePage(data)}
        />
    );
};

export default RentSectionForm;