import React from "react";
import PrefetchingForm from "../../common/forms/PrefetchingForm";
import {getSitemap, updateSitemap} from "../../api/http/static-files";

const SitemapForm = () => {
    return (
        <PrefetchingForm
            fields={{
                content: {
                    type: 'code',
                    options: {
                        name: 'sitemapTxtAceEditor',
                        mode: 'xml',
                    },
                },
            }}
            prefetchOptions={getSitemap()}
            usePrefetchCaching
            cacheExtrasOnly
            prefetchCacheKey={'sitemapForm'}
            submitOptions={data => updateSitemap(data)}
        />
    );
};

export default SitemapForm;