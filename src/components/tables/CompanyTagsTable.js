import React from "react";
import DataTable from "../../common/tables/DataTable";
import {deleteCompanyTag, listCompanyTags, moveNextCompanyTag, movePrevCompanyTag} from "../../api/http/company-tag";
import CreateCompanyTagForm from "../forms/CreateCompanyTagForm";
import UpdateCompanyTagForm from "../forms/UpdateCompanyTagForm";

const CompanyTagTable = props => {
    return (
        <DataTable
            header="Виды деятельности"
            columns={{
                name: {},
            }}
            fetchOptions={listCompanyTags()}
            createItemForm={CreateCompanyTagForm}
            updateItemForm={UpdateCompanyTagForm}
            moveUpOptions={({id}) => moveNextCompanyTag(id)}
            moveDownOptions={({id}) => movePrevCompanyTag(id)}
            deleteItemOptions={({id}) => deleteCompanyTag(id)}
            {...props}
        />
    );
};

export default CompanyTagTable;