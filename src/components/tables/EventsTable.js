import React from "react";
import DataTable from "../../common/tables/DataTable";
import {listEvents} from "../../api/http/events";
import CreateEventForm from "../forms/CreateEventForm";

const EventsTable = props => {
    return (
        <DataTable
            header="Мероприятия"
            columns={{
                name: {
                    options: {
                        includeVisibilityIcon: true,
                    },
                },
                archive_date: {},
                show_on_home_page: {
                    label: 'Выводится на главную',
                    type: 'boolean',
                    options: {
                        renderAs: 'checkbox',
                    },
                },
            }}
            fetchOptions={listEvents()}
            createItemForm={CreateEventForm}
            updateItemRoute={'/content/events/update'}
            sorting
            {...props}
        />
    );
};

export default EventsTable;