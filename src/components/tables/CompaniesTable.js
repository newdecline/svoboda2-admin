import React from "react";
import DataTable from "../../common/tables/DataTable";
import {listCompanies} from "../../api/http/companies";
import CreateCompanyForm from "../forms/CreateCompanyForm";

const CompaniesTable = props => {
    return (
        <DataTable
            header="Жители"
            columns={{
                name: {
                    options: {
                        includeVisibilityIcon: true,
                    },
                },
                location: {},
            }}
            fetchOptions={listCompanies()}
            createItemForm={CreateCompanyForm}
            updateItemRoute={'/content/companies/update'}
            sorting
            {...props}
        />
    );
};

export default CompaniesTable;