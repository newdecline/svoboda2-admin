import React from "react";
import DataTable from "../../common/tables/DataTable";
import {listRooms, moveNextRoom, movePrevRoom} from "../../api/http/rooms";
import CreateRoomForm from "../forms/CreateRoomForm";

const RoomsTable = props => {
    return (
        <DataTable
            header="Помещения"
            columns={{
                name: {
                    options: {
                        includeVisibilityIcon: true,
                    },
                },
                location: {},
                square: {},
                cost: {},
                show_on_home_page: {
                    label: 'Выводится на главную',
                    type: 'boolean',
                    options: {
                        renderAs: 'checkbox',
                    },
                },
            }}
            fetchOptions={listRooms()}
            createItemForm={CreateRoomForm}
            updateItemRoute={'/content/rooms/update'}
            moveUpOptions={({id}) => moveNextRoom(id)}
            moveDownOptions={({id}) => movePrevRoom(id)}
            sorting
            {...props}
        />
    );
};

export default RoomsTable;