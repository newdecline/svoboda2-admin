import React from "react";
import DataTable from "../../common/tables/DataTable";
import {listAreas, moveNextArea, movePrevArea} from "../../api/http/areas";
import CreateAreaForm from "../forms/CreateAreaForm";

const AreasTable = props => {
    return (
        <DataTable
            header="Площадки"
            columns={{
                name: {
                    options: {
                        includeVisibilityIcon: true,
                    },
                },
            }}
            fetchOptions={listAreas()}
            createItemForm={CreateAreaForm}
            updateItemRoute={'/content/areas/update'}
            moveUpOptions={({id}) => moveNextArea(id)}
            moveDownOptions={({id}) => movePrevArea(id)}
            {...props}
        />
    );
};

export default AreasTable;