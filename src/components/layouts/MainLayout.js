import React from 'react';
import {Route, Switch} from 'react-router-dom';
import {makeStyles} from "@material-ui/core/styles";
import {Box, Slide} from "@material-ui/core";
import Header from "./parts/Header";
import Drawer from "./parts/Drawer";
import NavBar from "./parts/NavBar";

const useStyles = makeStyles(() => ({
    wrapper: {
        display: 'flex',
        flexGrow: 1,
        flexShrink: 1,
        flexBasis: 'auto',
    },
    content: {
        width: '100%',
        padding: '37px 39px',
    },
}));

const MainLayout = ({content: Content, drawer, path: layoutPath}) => {
    const styles = useStyles();

    const drawerRoutes = drawer && drawer.reduce((a, b) => [
        ...a,
        ...[{...b, path: layoutPath + b.path}],
        ...(b.children ? b.children.map(child => ({...child, nested: true, path: layoutPath + b.path + child.path})) : [])
    ], []);

    return (
        <>
            <Header />
            <NavBar />
            <div className={styles.wrapper}>
                {drawer &&
                <Slide in direction={'right'}>
                    <Box height={1}>
                        <Drawer items={drawerRoutes} />
                    </Box>
                </Slide>
                }
                <div className={styles.content}>
                    {Content && !drawer && <Content />}
                    {drawer &&
                    <Switch>
                        {Content && <Route key="root" path={layoutPath} component={Content} exact={true} />}
                        {drawerRoutes.map((props, i) => <Route key={i} path={props.path} component={props.page} exact={props.exact} />)}
                    </Switch>
                    }
                </div>
            </div>
        </>
    );
};

export default MainLayout;