import React from 'react';
import {useSelector} from "react-redux";
import {Redirect} from "react-router-dom";
import {isAuthorized} from "../../store/reducers/rootReducer";
import {makeStyles} from "@material-ui/core/styles";
import LoginForm from "../forms/LoginForm";

const useStyles = makeStyles(theme => ({
    wrap: {
        width: 'auto',
        display: 'block', // Fix IE 11 issue.
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),
        marginTop: 'auto',
        marginBottom: 'auto',
        [theme.breakpoints.up(400 + theme.spacing(3) * 2)]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
}));

const LoginLayout = () => {
    const styles = useStyles();

    const isUserAuthorized = useSelector(isAuthorized);

    return isUserAuthorized ? (
        <Redirect to="/" />
    ) : (
        <div className={styles.wrap}>
            <LoginForm />
        </div>
    );
};

export default LoginLayout;