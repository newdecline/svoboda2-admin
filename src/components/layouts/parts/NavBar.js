import React from 'react';
import {Link, withRouter} from "react-router-dom";
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
    paper: {
        boxShadow: '0 4px 4px rgba(0, 0, 0, 0.24), 0 0 4px rgba(0, 0, 0, 0.12)',
        zIndex: 1,
        width: '100%',
    },
}));

const tabs = [
    {
        label: 'Контент',
        value: '/content',
    },
    {
        label: 'Модули',
        value: '/modules',
    },
    {
        label: 'SEO',
        value: '/seo',
    },
    {
        label: 'Пользователи',
        value: '/users',
    },
];

const LinkTab = props =>
    <Tab
        component={Link}
        to={props.value}
        {...props}
    />;

const NavBar = props => {
    const styles = useStyles();
    const {history} = props;

    const activeTab = '/' + history.location.pathname.split('/')[1];

    return (
        <Paper square className={styles.paper}>
            <Tabs
                centered
                value={activeTab === '/' ? false : activeTab}
                indicatorColor="primary"
                textColor="primary"
            >
                {tabs.map(tab => <LinkTab key={tab.value} {...tab} />)}
            </Tabs>
        </Paper>
    );
};

export default withRouter(NavBar);