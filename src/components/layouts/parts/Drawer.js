import React from 'react';
import {Link, withRouter} from 'react-router-dom';
import {default as MuiDrawer} from '@material-ui/core/Drawer';
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    wrap: {
        boxShadow: '2px 0 8px -2px rgba(0, 0, 0, 0.24), 0 0 0 rgba(0, 0, 0, 0.18)',
        backgroundColor: 'white',
        height: '100%',
    },
    drawerPaper: {
        position: 'static',
        minWidth: 270,
        border: 'none',
    },
    nested: {
        paddingLeft: theme.spacing(5),
    },
    text: {
        textDecoration: 'none',
    },
}));

const DrawerItem = withRouter(({path, label, icon, nested, location: {pathname}}) => {
    const styles = useStyles();

    return (
        <ListItem
            button
            className={nested && styles.nested}
            to={path}
            component={Link}
            selected={path === pathname}
        >
            <ListItemIcon>{icon}</ListItemIcon>
            <ListItemText className={styles.text} primary={label} />
        </ListItem>
    );
});

const Drawer = ({items}) => {
    const styles = useStyles();

    return (
        <div className={styles.wrap}>
            <MuiDrawer
                classes={{paper: styles.drawerPaper}}
                variant="permanent"
            >
                <List>
                    {items.map((props, i) => !props.hidden && <DrawerItem key={i} {...props} />)}
                </List>
            </MuiDrawer>
        </div>
    );
};

export default Drawer;