import React, {useState} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import {Link} from "react-router-dom";
import {useDispatch} from 'react-redux';
import {logout} from "../../../store/actions/authorization";
import useHttpApi from "../../../hooks/useHttpApi";

const useStyles = makeStyles(() => ({
    appBar: {
        boxShadow: '0 4px 4px rgba(0, 0, 0, 0.24), 0 0 4px rgba(0, 0, 0, 0.12)',
    },
    root: {
        flexGrow: 1,
    },
    grow: {
        flexGrow: 1,
        textDecoration: 'none',
        color: '#fff'
    },
    bottom: {
        padding: 0
    },
    link: {
        color: 'black',
        textDecoration: 'none'
    }
}));

const Header = () => {
    const styles = useStyles();
    const dispatch = useDispatch();
    const {makeRequest} = useHttpApi();

    const [accountMenuAnchor, setAccountMenuAnchor] = useState(null);

    const handleAccountMenuOpen = event => setAccountMenuAnchor(event.currentTarget);
    const handleAccountMenuClose = () => setAccountMenuAnchor(null);

    const handleLogout = async () => {
        dispatch(logout(makeRequest));
    };

    const accountMenuIsOpen = Boolean(accountMenuAnchor);

    return (
        <div>
            <div className={styles.root}>
                <AppBar position="static" className={styles.appBar}>
                    <Toolbar>
                        <Typography variant="h6" color="inherit" className={styles.grow}>
                            <Link to='/' className={styles.grow}>
                                {window.location.hostname}
                            </Link>
                        </Typography>
                        <div>
                            <IconButton
                                aria-owns={accountMenuIsOpen ? 'account_menu' : undefined}
                                aria-haspopup="true"
                                onClick={handleAccountMenuOpen}
                                color="inherit"
                            >
                                <AccountCircle />
                            </IconButton>
                            <Menu
                                id="account_menu"
                                anchorEl={accountMenuAnchor}
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                open={accountMenuIsOpen}
                                onClose={handleAccountMenuClose}
                            >
                                <MenuItem onClick={handleAccountMenuClose}>
                                    <Link to='/profile' className={styles.link}>Профиль</Link>
                                </MenuItem>
                                <MenuItem onClick={handleLogout}>
                                    Выйти
                                </MenuItem>
                            </Menu>
                        </div>
                    </Toolbar>
                </AppBar>
            </div>
        </div>
    );
};

export default Header;