import {useSnackbar} from 'notistack';
import {useDispatch, useSelector} from "react-redux";
import {getAuthToken} from "../store/reducers/rootReducer";
import {setIsAuthorized} from "../store/actions/authorization";

const API_BASE = '/api';

const useHttpApi = () => {
    const {enqueueSnackbar} = useSnackbar();
    const authToken = useSelector(getAuthToken);
    const dispatch = useDispatch();
    
    const makeRequest = async (options) => {
        const defaults = {
            method: 'get',
            body: {},
            query: {},
            contentType: 'application/json',
            snackbar: true,
            fields: [],
            expand: [],
            authToken: null,
        };

        const {
            route,
            method,
            body,
            query,
            contentType,
            snackbar,
            fields,
            expand,
            authToken: authTokenFromOptions,
        } = {...defaults, ...options};

        const config = {
            headers: {
                'Authorization' : `Bearer ${authTokenFromOptions || authToken}`,
            },
            method,
        };
        if (contentType) {
            config.headers['Content-Type'] = contentType;
        }
        if (method !== 'get' && method !== 'head') {
            config.body = contentType ==='application/json' ? JSON.stringify(body) : body;
        }

        const urlObject = new URL(`${API_BASE}${route}`, window.location.origin);
        for (const key in query) {
            urlObject.searchParams.append(key, query[key])
        }
        fields.length && urlObject.searchParams.append('fields', fields.join(','));
        expand.length && urlObject.searchParams.append('expand', expand.join(','));

        const response = await fetch(urlObject, config);

        switch (response.status) {
            case 200: case 201: case 204:
            snackbar && method !== 'get' && enqueueSnackbar('Изменения сохранены');
            break;
            case 401:
                // enqueueSnackbar('Ошибка авторизации', {variant: 'error'});
                dispatch(setIsAuthorized(false));
                break;
            case 403:
                snackbar && enqueueSnackbar('Доступ запрещен', {variant: 'error'});
                break;
            case 422:
                snackbar && enqueueSnackbar('Ошибка при заполнении формы', {variant: 'error'});
                break;
            case 400: case 404: case 405: case 415: case 429: case 500:
            snackbar && enqueueSnackbar('Ошибка', {variant: 'error'});
            break;
            default:
                break;
        }

        return {
            status: response.status,
            data: response.status === 204 ? null : await response.json(),
        }
    };

    return {makeRequest};
};

export default useHttpApi;