import React from "react";
import MainLayout from "./components/layouts/MainLayout";
import LoginLayout from "./components/layouts/LoginLayout";
import DashboardPage from "./components/pages/DashboardPage";
import ProfilePage from "./components/pages/ProfilePage";
import LayoutPage from "./components/pages/content/LayoutPage";
import SeoPage from "./components/pages/seo/SeoPage";
import CodeIcon from '@material-ui/icons/Code';
import CustomCodePage from "./components/pages/seo/CustomCodePage";
import HomePage from "./components/pages/content/HomePage";
import HomeIcon from '@material-ui/icons/Home';
import SettingsIcon from '@material-ui/icons/Settings';
import MetaTagsPage from "./components/pages/seo/MetaTagsPage";
import RobotsPage from "./components/pages/seo/RobotsPage";
import SitemapPage from "./components/pages/seo/SitemapPage";
import DescriptionIcon from '@material-ui/icons/Description';
import InfoIcon from '@material-ui/icons/Info';
import ModulesPage from "./components/pages/widgets/ModulesPage";
import GalleriesPage from "./components/pages/widgets/GalleriesPage";
import FeedbackFormsPage from "./components/pages/widgets/FeedbackFormsPage";
import PhotoLibraryIcon from '@material-ui/icons/PhotoLibrary';
import MessageIcon from '@material-ui/icons/Message';
import UsersPage from "./components/pages/users/UsersPage";
import TopSectionPage from "./components/pages/content/home-page/TopSectionPage";
import AboutSectionPage from "./components/pages/content/home-page/AboutSectionPage";
import ContactsSectionPage from "./components/pages/content/home-page/ContactsSectionPage";
import ViewQuiltIcon from '@material-ui/icons/ViewQuilt';
import AlternateEmailIcon from '@material-ui/icons/AlternateEmail';
import EventsSectionPage from "./components/pages/content/home-page/EventsSectionPage";
import EventIcon from "@material-ui/icons/Event";
import EventSeatIcon from "@material-ui/icons/EventSeat";
import DateRangeIcon from "@material-ui/icons/DateRange";
import OwnEventSectionPage from "./components/pages/content/home-page/OwnEventSectionPage";
import EventsPage from "./components/pages/content/EventsPage";
import UpdateEventPage from "./components/pages/UpdateEventPage";
import AreasSectionPage from "./components/pages/content/home-page/AreasSectionPage";
import UpdateAreaPage from "./components/pages/UpdateAreaPage";
import ViewModuleIcon from '@material-ui/icons/ViewModule';
import RentPage from "./components/pages/content/RentPage";
import UpdateRoomPage from "./components/pages/UpdateRoomPage";
import GroupIcon from '@material-ui/icons/Group';
import RentSectionPage from "./components/pages/content/home-page/RentSectionPage";
import CompaniesSectionPage from "./components/pages/content/home-page/CompaniesSectionPage";
import CompaniesPage from "./components/pages/content/CompaniesPage";
import UpdateCompanyPage from "./components/pages/UpdateCompanyPage";

const routes = [
    {
        path: "/",
        exact: true,
        layout: {
            component: MainLayout,
            props: {
                content: DashboardPage,
            },
        },
    },
    {
        path: "/login",
        layout: {
            component: LoginLayout,
        },
        isPublic: true,
    },
    {
        path: "/profile",
        layout: {
            component: MainLayout,
            props: {
                content: ProfilePage,
            },
        },
    },
    {
        path: "/content",
        layout: {
            component: MainLayout,
            props: {
                content: LayoutPage,
                drawer: [
                    {
                        path: "/home-page",
                        exact: true,
                        label: "Главная страница",
                        icon: <HomeIcon />,
                        page: HomePage,
                        children: [
                            {
                                path: "/top",
                                label: "Верхняя секция",
                                icon: <ViewQuiltIcon />,
                                page: TopSectionPage,
                            },
                            {
                                path: "/about",
                                label: "О компании",
                                icon: <InfoIcon />,
                                page: AboutSectionPage,
                            },
                            {
                                path: "/events",
                                label: 'Секция "Афиша"',
                                icon: <DateRangeIcon />,
                                page: EventsSectionPage,
                            },
                            {
                                path: "/own-event",
                                label: "Своё мероприятие",
                                icon: <EventIcon />,
                                page: OwnEventSectionPage,
                            },
                            {
                                path: "/areas",
                                label: "Площадки",
                                icon: <EventSeatIcon />,
                                page: AreasSectionPage,
                            },
                            {
                                path: "/rent",
                                label: 'Секция "Аренда"',
                                icon: <ViewModuleIcon />,
                                page: RentSectionPage,
                            },
                            {
                                path: "/companies",
                                label: 'Секция "Жители"',
                                icon: <GroupIcon />,
                                page: CompaniesSectionPage,
                            },
                            {
                                path: "/contacts",
                                label: "Контакты",
                                icon: <AlternateEmailIcon />,
                                page: ContactsSectionPage,
                            },
                        ],
                    },
                    {
                        path: "/events",
                        exact: true,
                        label: "Афиша",
                        icon: <EventIcon />,
                        page: EventsPage,
                    },
                    {
                        path: "/rent",
                        exact: true,
                        label: "Аренда",
                        icon: <ViewModuleIcon />,
                        page: RentPage,
                    },
                    {
                        path: "/companies",
                        exact: true,
                        label: "Жители",
                        icon: <GroupIcon />,
                        page: CompaniesPage,
                    },

                    // Hidden
                    {
                        path: "/events/update",
                        exact: true,
                        page: UpdateEventPage,
                        hidden: true,
                    },
                    {
                        path: "/areas/update",
                        exact: true,
                        page: UpdateAreaPage,
                        hidden: true,
                    },
                    {
                        path: "/rooms/update",
                        exact: true,
                        page: UpdateRoomPage,
                        hidden: true,
                    },
                    {
                        path: "/companies/update",
                        exact: true,
                        page: UpdateCompanyPage,
                        hidden: true,
                    },
                ],
            },
        },
    },
    {
        path: "/seo",
        layout: {
            component: MainLayout,
            props: {
                content: SeoPage,
                drawer: [
                    {
                        path: "/custom-code",
                        label: "Сторонний код",
                        icon: <CodeIcon />,
                        page: CustomCodePage,
                    },
                    {
                        path: "/meta-tags",
                        label: "Мета теги",
                        icon: <SettingsIcon />,
                        page: MetaTagsPage,
                    },
                    {
                        path: "/robots",
                        label: "robots.txt",
                        icon: <DescriptionIcon />,
                        page: RobotsPage,
                    },
                    {
                        path: "/sitemap",
                        label: "sitemap.xml",
                        icon: <DescriptionIcon />,
                        page: SitemapPage,
                    },
                ],
            },
        },
    },
    {
        path: "/modules",
        layout: {
            component: MainLayout,
            props: {
                content: ModulesPage,
                drawer: [
                    {
                        path: "/galleries",
                        label: "Галереи",
                        icon: <PhotoLibraryIcon />,
                        page: GalleriesPage,
                    },
                    {
                        path: "/feedback-forms",
                        label: "Формы обр. связи",
                        icon: <MessageIcon />,
                        page: FeedbackFormsPage,
                    },
                ],
            },
        },
    },
    {
        path: "/users",
        layout: {
            component: MainLayout,
            props: {
                content: UsersPage,
            },
        },
    },
    {
        path: "*",
        layout: {
            component: MainLayout,
            props: {
                content: DashboardPage,
            },
        },
    },
];

export default routes;