import React, {useState, useEffect} from "react";
import useHttpApi from "../../hooks/useHttpApi";
import {range} from "lodash";
import {makeStyles} from "@material-ui/core/styles";
import {
    GridList,
    GridListTile,
    GridListTileBar,
    IconButton,
    Menu,
    MenuItem,
    ListItemIcon,
    Typography,
    Card,
    CardContent,
    CardActions,
} from '@material-ui/core';
import Skeleton from "@material-ui/lab/Skeleton";
import MoreVertIcon from '@material-ui/icons/MoreVert';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import DeleteIcon from '@material-ui/icons/Delete';
import VisibilityIcon from '@material-ui/icons/Visibility';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import EditIcon from '@material-ui/icons/Edit';
import ConfirmModal from "../modals/ConfirmModal";
import Pagination from "../tables/Pagination";
import CardHeader from "../CardHeader";
import Modal from "../modals/Modal";
import CardProgress from "../CardProgress";
import NoItemsPlaceholder from "../NoItemsPlaceholder";

const GRID_LIST_COLUMNS_COUNT = 3;
const ITEM_MENU_ID = 'item-menu';
const DEFAULT_PAGE = 1;
const DEFAULT_PER_PAGE = 3;
const DEFAULT_PER_PAGE_OPTIONS = [1, 2, 3].map(factor => DEFAULT_PER_PAGE * factor);

const useStyles = makeStyles(theme => ({
    gridWrap: {
        padding: 0,
    },
    menuIcon: {
        color: 'rgba(255, 255, 255, 0.74)',
    },
    topTitleBar: {
        background:
        'linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, ' +
        'rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
    },
    hiddenIcon: {
        position: 'absolute',
        top: 'calc(50% - 50px)',
        left: 'calc(50% - 50px)',
        fontSize: 100,
        opacity: .5,
    },
    skeleton: {
        height: '100%',
        minWidth: 300,
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end',
    },
    button: {
        marginTop: theme.spacing(3),
    },
    cardActions: {
        justifyContent: 'flex-end',
    },
}));

const ImagesDataGrid = props => {
    const {
        id: parentId,
        forceLoading,
        fetchOptions,
        createItemForm: CreateItemForm,
        updateItemForm: UpdateItemForm,
        header,
        itemImage,
        itemIsHidden,
        tileBarTitle,
        moveRightItemOptions,
        moveLeftItemOptions,
        updateItemVisibilityOptions,
        deleteItemOptions,
        isModal,
        isOpen,
        onClose,
        formProps,
    } = props;

    const styles = useStyles();
    const {makeRequest} = useHttpApi();

    const [globalLoading, setGlobalLoading] = useState(true);
    const [data, setData] = useState(null);
    const [createItemFormIsOpen, setCreateItemFormIsOpen] = useState(false);
    const [currentPage, setCurrentPage] = useState(1);
    const [itemToEdit, setItemToEdit] = useState(null);
    const [itemToDelete, setItemToDelete] = useState(null);
    const [itemMenuAnchorEl, setItemMenuAnchorEl] = useState(null);
    const [itemMenu, setItemMenu] = useState([]);
    const [perPage, setPerPage] = useState(DEFAULT_PER_PAGE);
    const [loading, setLoading] = useState(false);

    const fetchPage = async (page, perPage, firstTime = false) => {
        firstTime ? setGlobalLoading(true) : setLoading(true);

        let query = fetchOptions.query || {};

        query.page = page;
        query['per-page'] = perPage;

        const response = await makeRequest({
            ...fetchOptions,
            ...{
                expand: ['_tableMeta'],
                query,
            }
        });
        if (response.status === 200) {
            setData(response.data);
            setCurrentPage(response.data._tableMeta.pagination.currentPage);
            setPerPage(response.data._tableMeta.pagination.perPage);
        }

        firstTime ? setGlobalLoading(false) : setLoading(false);
    };

    useEffect(() => {
        const fetchData = async () => {
            await fetchPage(DEFAULT_PAGE, perPage, true);
        };

        !forceLoading && fetchData();
    }, [forceLoading]);

    const handleChangePage = async page =>
        await fetchPage(page, perPage);

    const handleChangeRowsPerPage = async event =>
        await fetchPage(DEFAULT_PAGE, parseInt(event.target.value, 10));

    const handleCreateItemFormSuccess = async () => {
        setCreateItemFormIsOpen(false);
        await fetchPage(DEFAULT_PAGE, perPage);
    };

    const handleUpdateItemFormSuccess = async () => {
        setItemToEdit(null);
        await fetchPage(currentPage, perPage);
    };

    const handleItemMoveRight = async item => {
        const response = await makeRequest(moveRightItemOptions(item));
        response.status === 200 && await fetchPage(currentPage, perPage);
    };
    const handleItemMoveLeft = async item => {
        const response = await makeRequest(moveLeftItemOptions(item));
        response.status === 200 && await fetchPage(currentPage, perPage);
    };

    const handleItemChangeVisibility = async item => {
        const response = await makeRequest(updateItemVisibilityOptions(item, !item[itemIsHidden]));
        response.status === 200 && await fetchPage(currentPage, perPage);
    };

    const handleItemDelete = item => {
        setItemToDelete(item);
    };

    const onDeleteItemModalConfirm = async () => {
        const response = await makeRequest(deleteItemOptions(itemToDelete));
        setItemToDelete(null);
        response.status === 204 && await fetchPage(currentPage, perPage);
    };

    const getItemMenu = item => [
        {
            label: 'Редактировать',
            icon: <EditIcon />,
            onClick: () => setItemToEdit(item),
        },
        {
            label: 'Переместить вправо',
            icon: <ArrowForwardIcon />,
            onClick: () => handleItemMoveRight(item),
        },
        {
            label: 'Переместить влево',
            icon: <ArrowBackIcon />,
            onClick: () => handleItemMoveLeft(item),
        },
        {
            label: item[itemIsHidden] ? 'Показать' : 'Скрыть',
            icon: item[itemIsHidden] ? <VisibilityIcon /> : <VisibilityOffIcon />,
            onClick: () => handleItemChangeVisibility(item),
            itemProps: {divider: true},
        },
        {
            label: 'Удалить',
            icon: <DeleteIcon />,
            onClick: () => handleItemDelete(item),
            typographyProps: {color: 'error'},
        },
    ];

    const gridLoading = forceLoading || globalLoading;
    const gridListProps = {
        cellHeight: 300,
        cols: GRID_LIST_COLUMNS_COUNT,
    };

    const card = () =>
        <Card>
            <CardContent>
                <CardHeader
                    header={header}
                    addAction={() => setCreateItemFormIsOpen(true)}
                />
            </CardContent>
            <CardProgress loading={loading} />
            <CardContent className={styles.gridWrap}>
                {!gridLoading ? (
                    data.items.length ? (
                        <GridList {...gridListProps}>
                            {data.items.map(item =>
                                <GridListTile key={item.id}>
                                    <img
                                        title="Редактировать"
                                        src={item[itemImage].url}
                                        alt={item[itemImage].alt}
                                        style={{
                                            opacity: item[itemIsHidden] ? .5 : 1,
                                            cursor: 'pointer',
                                        }}
                                        onClick={() => setItemToEdit(item)}
                                    />
                                    {item[itemIsHidden] && <VisibilityOffIcon className={styles.hiddenIcon}/>}
                                    <GridListTileBar
                                        titlePosition="top"
                                        actionIcon={
                                            <IconButton
                                                aria-owns={ITEM_MENU_ID}
                                                aria-haspopup="true"
                                                onClick={
                                                    ({currentTarget}) => {
                                                        setItemMenu(getItemMenu(item));
                                                        setItemMenuAnchorEl(currentTarget);
                                                    }
                                                }
                                                className={styles.menuIcon}
                                            >
                                                <MoreVertIcon/>
                                            </IconButton>
                                        }
                                        className={styles.topTitleBar}
                                    />
                                    {tileBarTitle && <GridListTileBar title={tileBarTitle(item)}/>}
                                </GridListTile>
                            )}
                        </GridList>
                    ) : (
                        <NoItemsPlaceholder />
                    )
                ) : (
                    <GridList {...gridListProps}>
                        {range(GRID_LIST_COLUMNS_COUNT).map(key =>
                            <GridListTile key={key}>
                                <Skeleton variant="rect" className={styles.skeleton} />
                            </GridListTile>
                        )}
                    </GridList>
                )}
            </CardContent>
            {!gridLoading && (
                <CardActions className={styles.cardActions}>
                    <Pagination
                        data={data}
                        onChangePage={handleChangePage}
                        rowsPerPageOptions={DEFAULT_PER_PAGE_OPTIONS}
                        onChangeRowsPerPage={handleChangeRowsPerPage}
                    />
                </CardActions>
            )}
        </Card>;

    return (
        <>
            {isModal ? (
                <Modal open={isOpen} onClose={onClose}>
                    {card()}
                </Modal>
            ) : card()}

            <Menu
                id={ITEM_MENU_ID}
                anchorEl={itemMenuAnchorEl}
                open={Boolean(itemMenuAnchorEl && itemMenu.length)}
                onClose={() => setItemMenuAnchorEl(null)}
            >
                {itemMenu.map((config, key) => {
                    const {
                        onClick,
                        icon,
                        label,
                        itemProps,
                        typographyProps,
                    } = config;

                    return (
                        <MenuItem
                            {...itemProps}
                            key={key}
                            onClick={() => {
                                onClick();
                                setItemMenuAnchorEl(null);
                            }}
                        >
                            <ListItemIcon>
                                {icon}
                            </ListItemIcon>
                            <Typography {...typographyProps} variant="inherit">
                                {label}
                            </Typography>
                        </MenuItem>
                    );
                })}
            </Menu>

            {parentId &&
                <CreateItemForm
                    id={parentId}
                    isModal
                    isOpen={createItemFormIsOpen}
                    onClose={() => setCreateItemFormIsOpen(false)}
                    onSuccess={handleCreateItemFormSuccess}
                    {...formProps}
                />
            }
            {itemToEdit &&
                <UpdateItemForm
                    id={itemToEdit.id}
                    isModal
                    isOpen={Boolean(itemToEdit)}
                    onClose={() => setItemToEdit(null)}
                    onSuccess={handleUpdateItemFormSuccess}
                    {...formProps}
                />
            }

            <ConfirmModal
                message="Вы уверены, что хотите удалить элемент?"
                confirmButtonText="Удалить"
                confirmButtonColor="secondary"
                isOpen={Boolean(itemToDelete)}
                onCancel={() => setItemToDelete(null)}
                onConfirm={onDeleteItemModalConfirm}
            />
        </>
    );
};

export default ImagesDataGrid;