import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {isAuthorized, isAuthorizing} from "../store/reducers/rootReducer";
import {makeStyles} from "@material-ui/core/styles";
import CircularProgress from '@material-ui/core/CircularProgress';
import {BrowserRouter, Redirect, Route, Switch, useLocation} from 'react-router-dom';
import {changeLocation} from "../store/actions/history";

const useStyles = makeStyles(() => ({
    progressWrap: {
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
}));

const ConfiguredRoute = ({path, exact, layout: {component: Layout, props: layoutProps}, isPublic}) => {
    const isUserAuthorized = useSelector(isAuthorized);
    const location = useLocation();
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(changeLocation());
    }, [location]);

    return (
        <Route
            path={path}
            exact={exact}
            render={
                renderProps => (isPublic || isUserAuthorized)
                    ? <Layout path={path} {...layoutProps} {...renderProps} />
                    : <Redirect to="/login" />
            }
        />
    );
};

const Router = ({config}) => {
    const styles = useStyles();
    const isUserAuthorizing = useSelector(isAuthorizing);

    return isUserAuthorizing ? (
        <div className={styles.progressWrap}>
            <CircularProgress />
        </div>
    ) : (
        <BrowserRouter basename={'/admin'}>
            <Switch>
                {config.map((routeProps, i) => <ConfiguredRoute {...routeProps} key={i} />)}
            </Switch>
        </BrowserRouter>
    );
};

export default Router;