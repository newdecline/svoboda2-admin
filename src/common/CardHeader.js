import React from "react";
import Typography from "@material-ui/core/Typography";
import AddIcon from "@material-ui/icons/AddBox";
import IconButton from "@material-ui/core/IconButton";
import {makeStyles} from "@material-ui/core/styles/index";

const useStyles = makeStyles(theme => ({
    actions: {
        marginLeft: theme.spacing(1),
    },
}));

const CardHeader = ({header, gutterBottom, addAction}) => {
    const styles = useStyles();

    return (
        <Typography component="h2" variant="h6" gutterBottom={gutterBottom}>
            {header}
            {addAction && (
                <span className={styles.actions}>
                    <IconButton
                        title="Добавить"
                        color="primary"
                        onClick={addAction}
                    >
                        <AddIcon />
                    </IconButton>
                </span>
            )}
        </Typography>
    );
};

export default CardHeader;