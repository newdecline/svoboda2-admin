import React from "react";
import {
    Button,
    Typography,
    Card,
    CardContent,
    CardActions,
} from "@material-ui/core";
import {makeStyles} from "@material-ui/core";
import Modal from "./Modal";

const useStyles = makeStyles(theme => ({
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end',
    },
    button: {
        marginTop: theme.spacing(3),
    },
}));

const ConfirmModal = props => {
    const {
        message,
        confirmButtonText,
        confirmButtonColor,
        isOpen,
        onCancel,
        onConfirm,
    } = props;
    const styles = useStyles();

    return (
        <Modal open={isOpen} onClose={onCancel}>
            <Card className={styles.modalCard}>
                <CardContent>
                    <Typography>{message}</Typography>
                </CardContent>
                <CardActions className={styles.buttons}>
                    <Button color="primary" onClick={onCancel}>Отмена</Button>
                    <Button
                        variant="contained"
                        color={confirmButtonColor}
                        onClick={onConfirm}
                    >
                        {confirmButtonText}
                    </Button>
                </CardActions>
            </Card>
        </Modal>
    );
};

export default ConfirmModal;
