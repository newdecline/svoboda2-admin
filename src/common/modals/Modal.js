import React from "react";
import {Modal as MuiModal} from "@material-ui/core";
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    modal: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        padding: theme.spacing(3),
    },
    modalInner: {
        outline: 'none',
        overflow: 'auto',
        maxWidth: '100%',
        maxHeight: '100%',
        minWidth: 450,
    },
}));

const Modal = ({open, onClose, children}) => {
    const styles = useStyles();

    return (
        <MuiModal
            open={open}
            onClose={onClose}
            className={styles.modal}
        >
            <div className={styles.modalInner}>
                {children}
            </div>
        </MuiModal>
    );
};

export default Modal;