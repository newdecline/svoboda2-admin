import React from "react";
import PrefetchingForm from "./PrefetchingForm";

const PolymorphicForm = props => {
    const {
        type,
        fields,
        polymorphic,
        loading,
        header,
        layout,
    } = props;

    return (
        <PrefetchingForm
            {...props}
            header={(!loading && polymorphic[type].header) || header}
            fields={!loading ? {...fields, ...polymorphic[type].fields} : fields}
            layout={layout || polymorphic[type].layout}
        />
    );
};

export default PolymorphicForm;