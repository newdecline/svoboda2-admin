import React, {useState, useEffect} from "react";
import {
    Stepper as MuiStepper,
    Step,
    StepLabel,
    StepContent,
    StepButton,
} from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import Layout, {getUsedFields} from "../Layout";
import {isFunction} from "lodash";
import {makeStyles} from "@material-ui/core/styles/index";

const useStyles = makeStyles(() => ({
    verticalStepLabelContainer: {
        textAlign: 'left',
    },
    wrap: {
        marginTop: 12,
    },
}));

const Stepper = ({options: {steps, orientation, header}, renderField, formikProps}) => {
    const styles = useStyles();

    const {errors, touched} = formikProps;
    const resolvedOrientation = orientation || 'horizontal';

    const [activeStep, setActiveStep] = useState(0);

    // TODO Шагать только на не задизейбленный шаг.
    const nextStep = () => (activeStep < steps.length - 1) && setActiveStep(activeStep + 1);

    const isStepHasError = ({layout}) => getUsedFields(layout)
        .map(name => Boolean(errors[name] && touched[name]))
        .find(error => error);
    const stepsHasError = steps.map(isStepHasError);

    const isStepDisabled = ({disabled}) => disabled ? (
        isFunction(disabled) ? disabled(stepsHasError, formikProps) : disabled
    ) : false;
    const stepsDisabled = steps.map(isStepDisabled);

    useEffect(() => {
        if (stepsDisabled[activeStep]) {
            const enabledStep = stepsDisabled.findIndex(disabled => !disabled);
            enabledStep !== -1 && setActiveStep(enabledStep);
        }
    }, [activeStep, stepsDisabled]);

    const handleStepButtonClick = index => {
        setActiveStep(index);
    };

    const renderStepContent = ({layout}) => (
        <Layout
            layout={layout}
            renderField={(fieldName, additionalProps) => renderField(fieldName, {...additionalProps, nextStep})}
            formikProps={formikProps}
        />
    );

    const renderStep = ({label, layout, disabled, subLabel}, index) => {
        return (
            <Step key={index}>
                <StepButton
                    disabled={stepsDisabled[index]}
                    onClick={() => handleStepButtonClick(index)}
                    optional={subLabel && <Typography variant="caption">{subLabel}</Typography>}
                >
                    <StepLabel
                        error={stepsHasError[index]}
                        classes={orientation === 'vertical' && {
                            labelContainer: styles.verticalStepLabelContainer,
                        }}
                    >
                        {label}
                    </StepLabel>
                </StepButton>
                {orientation === 'vertical' && (
                    <StepContent>
                        {renderStepContent(steps[activeStep])}
                    </StepContent>
                )}
            </Step>
        );
    };

    return (
        <div className={styles.wrap}>
            {header && <InputLabel shrink>{header}</InputLabel>}
            <MuiStepper
                activeStep={activeStep}
                orientation={resolvedOrientation}
                nonLinear
            >
                {steps.map(renderStep)}
            </MuiStepper>
            {orientation === 'horizontal' && renderStepContent(steps[activeStep])}
        </div>
    );
};

export default Stepper;