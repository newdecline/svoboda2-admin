import React from "react";
import {Grid as MuiGrid} from "@material-ui/core";

const Grid = ({options: {items}, renderField}) => {

    const renderCell = (fieldName, key) => (
        <MuiGrid item xs key={key}>
            {fieldName ? renderField(fieldName) : null}
        </MuiGrid>
    );

    return (
        <MuiGrid container spacing={3}>
            {items.map((fieldName, key) => renderCell(fieldName, key))}
        </MuiGrid>
    );
};

export default Grid;