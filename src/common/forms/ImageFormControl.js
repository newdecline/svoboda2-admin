import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import {
    ButtonBase,
    GridListTileBar,
} from '@material-ui/core';
import FormControl from "./FormControl";
import Skeleton from '@material-ui/lab/Skeleton';

const DEFAULT_WIDTH = 640;
const DEFAULT_HEIGHT = 320;

const useStyles = makeStyles(() => ({
    formControl: {
        display: 'block',
    },
    container: {
        backgroundSize: 'contain',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundColor: 'black',
        position: 'relative',
    },
    tileBar: {
        textAlign: 'left',
    },
    topTileBar: {
        background:
        'linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, ' +
        'rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
    },
    topTileBarIcon: {
        color: 'rgba(255, 255, 255, 0.74)',
    },
    skeleton: {
        margin: 0,
    },
}));

const ImageFormControl = props => {
    const styles = useStyles();

    const {
        src,
        alt,
        onClick,
        actionIcon,
        width,
        height,
        loading,
    } = props;

    const scales = {
        width: width || DEFAULT_WIDTH,
        height: height || DEFAULT_HEIGHT,
    };
    const commonContainerProps = {
        className: styles.container,
        style: {
            backgroundImage: src && `url('${src}')`,
            ...scales,
        },
    };

    const TileBar = () => <GridListTileBar className={styles.tileBar} title={`Alt: ${alt}`} />;
    const TopTileBar = () =>
        <GridListTileBar
            className={styles.topTileBar}
            titlePosition="top"
            actionIcon={actionIcon(styles.topTileBarIcon)}
        />;

    return (
        <FormControl {...props} className={styles.formControl} customContent>
            {loading ? (
                <Skeleton style={scales} className={styles.skeleton} />
            ) : (
                onClick ?
                    <ButtonBase {...{...commonContainerProps, onClick}}>
                        {alt && <TileBar />}
                    </ButtonBase> :
                    <div {...commonContainerProps}>
                        {actionIcon && <TopTileBar />}
                        {alt && <TileBar />}
                    </div>
            )}
        </FormControl>
    );
};

export default ImageFormControl;