import React from "react";
import {
    FormControl as MuiFormControl,
    InputLabel,
    FormHelperText,
} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import Skeleton from '@material-ui/lab/Skeleton';

const useStyles = makeStyles(() => ({
    childrenWrap: {
        paddingTop: 16,
    },
    labelSkeleton: {
        height: 12,
        maxWidth: 60,
        marginTop: 0,
        marginBottom: 4,
    },
}));

const FormControl = props => {
    const {
        name,
        label,
        error,
        touched,
        helperTextId,
        customContent,
        children,
        className,
        validators,
        loading,
        disabled,
    } = props;
    const styles = useStyles();
    const required = validators && Boolean(validators.find(item => item.validator === 'required_validator'));

    return (
        <MuiFormControl
            className={className}
            margin="none"
            fullWidth
            error={Boolean(error && touched)}
            disabled={disabled}
        >
            {loading ? (
                <Skeleton className={styles.labelSkeleton} />
            ) : (
                label && <InputLabel htmlFor={name} shrink={customContent} required={required}>{label}</InputLabel>
            )}

            {customContent ? (
                <div className={label && !loading ? styles.childrenWrap : null}>
                    {children}
                </div>
            ) : children}

            <FormHelperText id={helperTextId || `${name}-error-text`}>
                {error && touched && error}
            </FormHelperText>
        </MuiFormControl>
    );
};

export default FormControl;