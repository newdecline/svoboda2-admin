import React from "react";
import AceEditor from "react-ace";
import Skeleton from "@material-ui/lab/Skeleton";
import {makeStyles} from "@material-ui/core/styles";

import 'brace/mode/xml';
import 'brace/mode/html';
import 'brace/theme/xcode';
import FormControl from "../FormControl";

const useStyles = makeStyles(() => ({
    skeleton: {
        height: 500,
        margin: 0,
    },
}));

const CodeField = props => {
    const {
        name,
        value,
        handleChange,
        handleBlur,
        setFieldValue,
        options,
        loading,
    } = props;
    const styles = useStyles();

    return (
        <FormControl {...props} customContent>
            {loading ?
                <Skeleton className={styles.skeleton} /> :
                <AceEditor
                    theme="xcode"
                    onChange={newValue => setFieldValue(name, newValue)}
                    fontSize={14}
                    showPrintMargin={false}
                    showGutter={true}
                    highlightActiveLine={true}
                    value={value}
                    editorProps={{ $blockScrolling: Infinity }}
                    setOptions={{
                        showLineNumbers: true,
                        tabSize: 2,
                    }}
                    width="100%"
                    {...options}
                />
            }
            <input
                type="hidden"
                name={name}
                value={value}
                onChange={handleChange}
                onBlur={handleBlur}
            />
        </FormControl>
    );
};

export default CodeField;