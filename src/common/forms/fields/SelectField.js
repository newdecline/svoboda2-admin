import React, {useEffect, useState} from "react";
import Select from '@material-ui/core/Select';
import {makeStyles} from "@material-ui/core/styles";
import Skeleton from "@material-ui/lab/Skeleton";
import FormControl from "../FormControl";
import Input from "@material-ui/core/Input";
import useHttpApi from "../../../hooks/useHttpApi";
import MenuItem from "@material-ui/core/MenuItem";
import Checkbox from "@material-ui/core/Checkbox";
import ListItemText from "@material-ui/core/ListItemText";

const useStyles = makeStyles(() => ({
    skeleton: {
        height: 32,
        margin: 0,
    },
}));

const SelectField = props => {
    const {makeRequest} = useHttpApi();
    const {
        name,
        options: {fetchOptions, multiple},
        value,
        handleChange,
        handleBlur,
        loading,
    } = props;
    const styles = useStyles();
    const helperTextId = `${name}-error-text`;

    const [options, setOptions] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            const response = await makeRequest(fetchOptions);
            response.status === 200 && setOptions(response.data);
        };

        fetchData();
    }, []);

    return (
        <FormControl {...props} helperTextId={helperTextId}>
            {(loading || options === null) ? (
                <Skeleton className={styles.skeleton} />
            ) : (
                <Select
                    name={name}
                    multiple={multiple}
                    value={value}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    input={<Input />}
                    renderValue={selected => options.filter(item => selected.includes(item.id)).map(item => item.name).join(', ')}
                >
                    {options.map(({id, name}) => (
                        <MenuItem key={id} value={id}>
                            <Checkbox checked={value && value.includes(id)} />
                            <ListItemText primary={name} />
                        </MenuItem>
                    ))}
                </Select>
            )}
        </FormControl>
    );
};

export default SelectField;