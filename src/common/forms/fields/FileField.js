import React, {useState, useCallback, useEffect} from "react";
import {makeStyles} from "@material-ui/core/styles/index";
import {getFileExtension, getFileName} from "../../../services/file";
import useHttpApi from "../../../hooks/useHttpApi";
import {createFile} from "../../../api/http/files";
import {useDropzone} from "react-dropzone";
import ImageFormControl from "../ImageFormControl";
import {IconButton, CircularProgress} from '@material-ui/core';
import CancelIcon from '@material-ui/icons/Cancel';
import FormControl from "../FormControl";
import Skeleton from '@material-ui/lab/Skeleton';
import {get, entries} from "lodash";

const useStyles = makeStyles(theme => ({
    dropZone: {
        minWidth: 298,
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: 20,
        borderWidth: 2,
        borderRadius: 2,
        borderColor: theme.palette.text.disabled,
        borderStyle: 'dashed',
        backgroundColor: theme.palette.background.default,
        color: theme.palette.text.secondary,
        outline: 'none',
        transition: 'border .24s ease-in-out',
        cursor: 'pointer',
        '&:focus': {
            borderColor: theme.palette.primary.main,
        },
        '&.disabled': {
            opacity: .6,
        },
    },
    dropZoneError: {
        borderColor: theme.palette.error.main + ' !important',
        color: theme.palette.error.main,
    },
    skeleton: {
        height: 92,
        margin: 0,
    },
}));

/**
 * value example: {
 *     id: 1,                       // id модели File
 *     extension: "jpg",
 *     size: 10000,                 // размер в байтах
 *     url: "/images/example.jpg",  // url модели Image|локально созданный url из файла
 *     mime_type: "image/jpeg",
 *     name: "example",
 *
 *     file: File,                  // выбранный файл для загрузки
 * }
 */
const FileField = props => {
    const styles = useStyles();
    const {makeRequest} = useHttpApi();

    const {
        name,
        error,
        touched,
        handleBlur,
        setFieldValue,
        setFieldError,
        setFieldTouched,
        loading: formLoading,
        value,
        fieldsConfig,
        nextStep,
    } = props;

    const relatedCroppers = entries(fieldsConfig)
            .filter(([fieldName, config]) => config.type === 'cropper' && get(config, 'options.origin') === name)
            .map(([fieldName]) => fieldName);

    const [loading, setLoading] = useState(false);

    useEffect(() => {
        const uploadFile = async (value) => {
            // TODO Иногда пропускает.
            console.log('UPLOAD');

            setLoading(true);
            const response = await makeRequest(createFile(value.file));
            setLoading(false);

            if (response.status === 200) {
                setFieldValue(name, {
                    ...value,
                    id: response.data.id,
                });

                nextStep && nextStep();
            } else if (response.status === 422) {
                setFieldError(name, response.data[0].message);
            } else {
                setFieldError(name, 'Возникла ошибка при загрузке файла.');
            }
        };

        !error && value && !value.id && value.file && uploadFile(value);
    }, [value, error]);
    
    const onDrop = useCallback(acceptedFiles => {
        if (acceptedFiles.length > 0) {
            const acceptedFile = acceptedFiles[0];
            
            setFieldValue(name, {
                id: null,
                extension: getFileExtension(acceptedFile),
                size: acceptedFile.size,
                url: URL.createObjectURL(acceptedFile),
                mime_type: acceptedFile.type,
                name: getFileName(acceptedFile),

                file: acceptedFile,
            });
        }

        setFieldTouched(name, true, false);
    }, [name, setFieldTouched, setFieldValue]);

    const onFileDialogCancel = useCallback(() => setFieldTouched(name), [setFieldTouched, name]);

    const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop, onFileDialogCancel});

    const handleCancel = () => {
        for (let cropperName of relatedCroppers) {
            setFieldValue(cropperName, null);
        }
        setFieldValue(name, null);
    };

    const helperTextId = `${name}-error-text`;

    let dropZoneStyles = [styles.dropZone];
    touched && error && dropZoneStyles.push(styles.dropZoneError);

    const id = get(value, 'id');
    const preview = get(value, 'url');
    
    function renderImageFormControl() {
        return (
            <ImageFormControl
                {...props}
                src={preview}
                actionIcon={iconClass =>
                    <IconButton
                        onClick={handleCancel}
                        className={iconClass}
                    >
                        <CancelIcon />
                    </IconButton>
                }
            />
        );
    }

    function renderDropZoneFormControl() {
        return (
            <FormControl {...props} helperTextId={helperTextId} customContent>
                {formLoading ?
                    <Skeleton className={styles.skeleton} /> :
                    <div {...getRootProps({className: dropZoneStyles.join(' ')})}>
                        <input
                            aria-describedby={helperTextId}
                            name={name}
                            onBlur={handleBlur}
                            {...getInputProps()}
                        />
                        {loading ? <CircularProgress /> : (
                            isDragActive ?
                                <p>Отпустите файл ...</p> :
                                <p>Перетащите файл сюда или кликните</p>
                        )}
                    </div>
                }
            </FormControl>
        );
    }

    // TODO !error - костыль, потому что эффект пропускает загрузку до проставления ошибки.
    const renderField = () => id && !error ? renderImageFormControl() : renderDropZoneFormControl();
    
    return renderField();
};

export default FileField;