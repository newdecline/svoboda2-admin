import React from "react";
import {Input, InputAdornment, Tooltip} from "@material-ui/core";
import HelpIcon from "@material-ui/icons/HelpOutline";
import {makeStyles} from "@material-ui/core/styles";
import Skeleton from "@material-ui/lab/Skeleton";
import FormControl from "../FormControl";

const useStyles = makeStyles(() => ({
    hintAdornment: {
        cursor: 'default',
    },
    skeleton: {
        height: 32,
        margin: 0,
    },
}));

const HintAdornment = ({hint}) => {
    const styles = useStyles();

    return (
        <Tooltip disableFocusListener title={hint} placement="left">
            <InputAdornment position="end" className={styles.hintAdornment}>
                <HelpIcon color="action" />
            </InputAdornment>
        </Tooltip>
    );
};

const InputField = props => {
    const {
        name,
        options,
        value,
        handleChange,
        handleBlur,
        hint,
        loading,
    } = props;
    const styles = useStyles();
    const helperTextId = `${name}-error-text`;
    
    return (
        <FormControl {...props} helperTextId={helperTextId}>
            {loading ?
                <Skeleton className={styles.skeleton} /> :
                <Input
                    name={name}
                    aria-describedby={helperTextId}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={value}
                    endAdornment={hint && <HintAdornment hint={hint} />}
                    {...options}
                />
            }
        </FormControl>
    );
};

export default InputField;