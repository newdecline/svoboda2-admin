import React from "react";
import Skeleton from "@material-ui/lab/Skeleton";
import {KeyboardDateTimePicker} from "@material-ui/pickers";
import {makeStyles} from "@material-ui/core/styles";
import {get} from "lodash";

const useStyles = makeStyles(() => ({
    hintAdornment: {
        cursor: 'default',
    },
    skeleton: {
        height: 32,
        margin: 0,
    },
}));

const DEFAULT_FORMAT = 'DD.MM.YYYY HH:mm';

const DateTimeField = props => {
    const {
        name,
        value,
        handleChange,
        handleBlur,
        loading,
        label,
        error,
        touched,
        setFieldValue,
        setFieldTouched,
        validators,
    } = props;

    const styles = useStyles();
    const required = validators && Boolean(validators.find(item => item.validator === 'required_validator'));
    const dateValidator = validators && validators.find(item => item.validator === 'date_validator');
    const valueProps = value ? {inputValue: value} : {value: null};
    const resolvedFormat = get(dateValidator, 'options.format') || DEFAULT_FORMAT;

    const onChange = (date, value) => {
        handleChange(
            {
                target: {
                    name,
                    value,
                },
            },
            {
                format: resolvedFormat,
            },
            false
        );

        setFieldValue(name, value);
        setFieldTouched(name);
    };

    return loading ?
        <Skeleton className={styles.skeleton} /> :
        <KeyboardDateTimePicker
            name={name}
            label={label}
            inputValue={value}
            {...valueProps}
            required={required}
            error={error && touched}
            helperText={error && touched && error}
            onChange={onChange}
            onBlur={handleBlur}
            ampm={false}
            clearable
            autoOk
            invalidDateMessage={false}
            cancelLabel="Закрыть"
            clearLabel="Очистить"
            format={resolvedFormat}
        />;
};

export default DateTimeField;