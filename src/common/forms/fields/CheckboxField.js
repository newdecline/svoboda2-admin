import React from "react";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Skeleton from '@material-ui/lab/Skeleton';
import {omit} from "lodash";

// TODO Релализовать через FormControl (дописать FormControl с исп. FormControlLabel)

const CheckboxField = props => {
    const {
        name,
        options,
        label,
        value,
        handleChange,
        handleBlur,
        loading,
    } = props;

    const onChange = (event) => {
        if (options.invertValue) {
            event.target.checked = !event.target.checked;
        }

        return handleChange(event);
    };

    const domOptions = omit(options, ['invertValue', 'value']);

    return (
        loading ?
            <Skeleton height={32} width={200} /> :
            <FormControlLabel
                control={
                    <Checkbox
                        name={name}
                        color="primary"
                        onChange={onChange}
                        onBlur={handleBlur}
                        checked={options.invertValue ? !value : value}
                        {...domOptions}
                    />
                }
                label={label}
            />
    );
};

export default CheckboxField;