import React, {useRef, useEffect} from "react";
import FormControl from "../FormControl";
import Typography from '@material-ui/core/Typography';
import {get, pick} from "lodash";
import Cropper from 'react-cropper';
import {Button} from "@material-ui/core";
import {cropFile as cropFileRequest} from "../../../api/http/files";
import useHttpApi from "../../../hooks/useHttpApi";
import ImageFormControl from "../ImageFormControl";
import {IconButton} from '@material-ui/core';
import CancelIcon from '@material-ui/icons/Cancel';
import CropIcon from '@material-ui/icons/Crop';
import {makeStyles} from "@material-ui/core/styles/index";

const useStyles = makeStyles(theme => ({
    buttons: {
        width: 640,
        display: 'flex',
        justifyContent: 'center',
        padding: theme.spacing(1),
    },
}));

/**
 * value example: {
 *     id: 1,                       // id модели File
 *     extension: "jpg",
 *     size: 10000,                 // размер в байтах
 *     url: "/images/slug.jpg",     // url модели Image|локально созданный url из файла
 *     cropData: [
 *         x: 0,
 *         y: 1,
 *         width: 100,
 *         height: 100,
 *     ],
 * }
 */
const CropperField = props => {
    const {
        value,
        error,
        name,
        options: {origin},
        fieldsConfig,
        values,
        errors,
        setFieldValue,
        setFieldTouched,
        setFieldError,
        validators,
        nextStep,
    } = props;

    const cropper = useRef(null);
    const {makeRequest} = useHttpApi();
    const styles = useStyles();

    const helperTextId = `${name}-error-text`;
    const originLabel = fieldsConfig[origin].label;
    const originValue = values[origin];
    const hasValidOrigin = Boolean(originValue && originValue.id && !errors[origin]);
    const id = get(value, 'id');
    const preview = get(value, 'url');
    const imageIdValidator = validators.find(validator => validator.validator === 'image_id_validator');
    const imageIdValidatorOptions = imageIdValidator ? imageIdValidator.options : {};

    useEffect(() => {
        const cropFile = async (value) => {
            // TODO Иногда пропускает (в FileField).
            console.log('CROP');

            const response = await makeRequest(cropFileRequest(originValue.id, value.cropData));

            if (response.status === 200) {
                setFieldValue(name, {
                    ...value,
                    id: response.data.id,
                });

                nextStep && nextStep();
            } else {
                setFieldError(name, 'Возникла ошибка при обработке файла.');
            }
        };

        hasValidOrigin && !error && value && !value.id && value.cropData && cropFile(value);

    }, [value, error, hasValidOrigin]);

    const cropImage = async () => {
        const cropData = cropper.current.getData(true);

        cropper.current.getCroppedCanvas().toBlob(blob => {
            setFieldValue(name, {
                id: null,
                extension: originValue.extension,
                size: blob.size,
                url: URL.createObjectURL(blob),
                cropData: pick(cropData, ['x', 'y', 'width', 'height']),
            });
            setFieldTouched(name, true, false);
        }, originValue.mime_type);
    };

    const handleCancel = () => {
        setFieldValue(name, null);
    };

    function renderImageFormControl() {
        return (
            <ImageFormControl
                {...props}
                src={preview}
                actionIcon={iconClass =>
                    <IconButton
                        onClick={handleCancel}
                        className={iconClass}
                    >
                        <CancelIcon/>
                    </IconButton>
                }
            />
        );
    }

    function renderCropper() {
        const {
            aspectRatio,
        } = imageIdValidatorOptions;

        // TODO Можно ограничивать cropBox "на лету" в зависимости от допустимых размеров и текущего масштаба

        return (
            <>
                <Cropper
                    ref={cropper}
                    src={originValue.url}
                    style={{width: 640, height: 320}}
                    viewMode={2}
                    aspectRatio={aspectRatio || NaN}
                    minCropBoxWidth={50}
                    minCropBoxHeight={50}
                    autoCropArea={1}
                />
                <div className={styles.buttons}>
                    <Button
                        color="primary"
                        variant="contained"
                        endIcon={<CropIcon/>}
                        onClick={() => cropImage()}
                    >
                        Готово
                    </Button>
                </div>
            </>
        );
    }

    function renderCropperFormControl() {
        return (
            <FormControl {...props} helperTextId={helperTextId} customContent disabled={!hasValidOrigin}>
                {hasValidOrigin ? renderCropper() : <Typography>Сначала загрузите {originLabel}</Typography>}
            </FormControl>
        );
    }

    // TODO !error - костыль, потому что эффект пропускает загрузку до проставления ошибки (в FileField)
    return id && !error ? renderImageFormControl() : renderCropperFormControl();
};

export default CropperField;