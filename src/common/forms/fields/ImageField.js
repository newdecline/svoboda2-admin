import React, {useState} from "react";
import ImageFormControl from "../ImageFormControl";
import {isFunction} from "lodash";
import UpdateImageForm from "../../../components/forms/UpdateImageForm";

const ImageField = props => {
    const {name, value: {id, url, alt}, setFieldValue, options} = props;
    const [formIsOpen, setFormIsOpen] = useState(false);

    const onFormSuccess = data => {
        setFieldValue(name, data);
        setFormIsOpen(false);

        options && isFunction(options.onChange) && options.onChange(data);
    };

    return (
        <>
            <ImageFormControl
                {...props}
                src={url}
                alt={alt}
                onClick={() => setFormIsOpen(true)}
            />
            <UpdateImageForm
                id={id}
                isOpen={formIsOpen}
                onClose={() => setFormIsOpen(false)}
                onSuccess={onFormSuccess}
            />
        </>
    );
};

export default ImageField;