import React from "react";
import {build} from "../../services/object";
import {makeStyles} from "@material-ui/core";
import {defaultsDeep, keys, isFunction, mapValues, get} from "lodash";
import {
    Button,
    Card,
    CardContent,
    CardActions,
} from "@material-ui/core";
import {Form as FormikForm, Formik} from "formik";
import {createValidationSchema} from "../../services/validation";
import InputField from "./fields/InputField";
import CheckboxField from "./fields/CheckboxField";
import FileField from "./fields/FileField";
import ImageField from "./fields/ImageField";
import CodeField from "./fields/CodeField";
import useHttpApi from "../../hooks/useHttpApi";
import DateTimeField from "./fields/DateTimeField";
import CropperField from "./fields/CropperField";
import Layout from "./Layout";
import CardHeader from "../CardHeader";
import Modal from "../modals/Modal";
import SelectField from "./fields/SelectField";

const useStyles = makeStyles(() => ({
    cardActions: {
        justifyContent: 'flex-end',
    }
}));

const prepareFileValue = (value, fieldConfig, values) => {
    const initialImageFieldName = get(fieldConfig, 'options.initialImage');
    const initialImage = initialImageFieldName && values[initialImageFieldName];

    if (initialImage && initialImage.file) {
        const {id, extension, size, mime_type, name} = initialImage.file;

        return {
            id,
            extension,
            size,
            url: initialImage.url,
            mime_type,
            name,
        };
    }

    return value;
};

const prepareCropperValue = (value, fieldConfig, values) => {
    const initialImageFieldName = get(fieldConfig, 'options.initialImage');
    const initialImage = initialImageFieldName && values[initialImageFieldName];

    if (initialImage && initialImage.file) {
        const {id, extension, size} = initialImage.file;

        return {
            id,
            extension,
            size,
            url: initialImage.url,
        };
    }

    return value;
};

const defaultFieldConfig = {
    type: 'input',
    options: {},
    validators: [],
    nullValue: '',
    extractValue: value => value,
    prepareValue: value => value,
};

const defaultFieldConfigByType = {
    input: {
        component: InputField,
    },
    checkbox: {
        nullValue: false,
        component: CheckboxField,
    },
    file: {
        nullValue: null,
        component: FileField,
        extractValue: value => value && value.id ? value.id : null,
        prepareValue: prepareFileValue,
    },
    image: {
        nullValue: {},
        component: ImageField,
    },
    code: {
        component: CodeField,
    },
    // TODO Можно хранить в moment (и избежать костылей внутри)
    dateTime: {
        component: DateTimeField,
    },
    cropper: {
        nullValue: null,
        component: CropperField,
        extractValue: value => value && value.id ? value.id : null,
        prepareValue: prepareCropperValue,
    },
    select: {
        component: SelectField,
    },
};

const buildFieldsConfig = initialFieldsConfig =>
    build(initialFieldsConfig, initialConfig => {
        const config = defaultsDeep(initialConfig, defaultFieldConfig);

        return {...config, ...defaultFieldConfigByType[config.type]};
    });

const prepareValues = (values, fieldsConfig) => mapValues(fieldsConfig, (fieldConfig, fieldName) => {
    const value = values[fieldName];

    return fieldConfig.prepareValue(value, fieldConfig, values) || fieldConfig.nullValue;
});

const Form = props => {
    const {
        fields, 
        values: initialValues,
        header, 
        submitOptions, 
        onSuccess, 
        isModal, 
        isOpen, 
        onClose, 
        loading, 
        buttonText, 
        buttonProps,
        layout,
        onChangeMiddleware,
        fieldMiddleware,
    } = props;

    const styles = useStyles();
    const {makeRequest} = useHttpApi();

    const fieldsConfig = buildFieldsConfig(fields);
    const resolvedHeader = isFunction(header) ? header(initialValues) : header;
    const resolvedButtonText = buttonText || 'Сохранить';
    const resolvedLayout = layout || keys(fieldsConfig).map(fieldName => [fieldName]);

    const extractValues = values => mapValues(values,
        (value, fieldName) => fieldsConfig[fieldName].extractValue(value)
    );

    const handleSubmit = async (values, props) => {
        const {setSubmitting, setErrors, resetForm} = props;
        const response = await makeRequest(submitOptions(extractValues(values)));

        if (response.status === 200 || response.status === 201) {
            isFunction(onSuccess) && onSuccess(response.data);
            !isModal && resetForm(prepareValues(response.data, fieldsConfig));
        } else if (response.status === 422) {
            setErrors(response.data.reduce((errors, {field, message}) => ({...errors, ...{[field]: message}}), {}));
        }

        setSubmitting(false);
    };

    const resolveValue = event => {
        switch (event.target.type) {
            case 'checkbox':
                return event.target.checked;
            default:
                return event.target.value;
        }
    };

    const formikInitialProps = {
        initialValues: prepareValues(initialValues, fieldsConfig),
        validationSchema: createValidationSchema(fieldsConfig),
        onSubmit: handleSubmit,
        enableReinitialize: true,
    };

    const handleClickFormButton = (e, submitForm) => {
        e.preventDefault();
        submitForm();
    };

    const renderForm = () => (
        <Formik {...formikInitialProps}>
            {formikProps => {
                const {
                    errors,
                    touched,
                    values,
                    isSubmitting,
                    isValid,
                    submitForm,
                    handleChange,
                    setFieldValue,
                    setFieldTouched,
                } = formikProps;

                const updateField = (fieldName, value) => {
                    setFieldValue(fieldName, value);
                    setFieldTouched(fieldName);
                };

                const onChange = (event, options={}, callFormikHandler=true) => {
                    callFormikHandler && handleChange(event);

                    if (onChangeMiddleware && onChangeMiddleware[event.target.name]) {
                        onChangeMiddleware[event.target.name](
                            resolveValue(event),
                            updateField,
                            values,
                            options,
                            formikProps,
                        );
                    }
                };

                const renderField = (fieldName, additionalProps) => {
                    const config = fieldMiddleware && fieldMiddleware[fieldName]
                        ? fieldMiddleware[fieldName](fieldsConfig[fieldName], values)
                        : fieldsConfig[fieldName];

                    return (
                        <config.component
                            {...formikProps}
                            {...config}
                            handleChange={onChange}
                            key={fieldName}
                            name={fieldName}
                            loading={loading}
                            error={errors[fieldName]}
                            touched={touched[fieldName]}
                            value={values[fieldName]}
                            fieldsConfig={fieldsConfig}
                            initialValues={initialValues}
                            {...additionalProps}
                        />
                    );
                };

                return (
                    <FormikForm>
                        <Card className={isModal && styles.modalCard}>
                            <CardContent>
                                {resolvedHeader && <CardHeader header={resolvedHeader} gutterBottom />}
                                <Layout
                                    layout={resolvedLayout}
                                    renderField={renderField}
                                    formikProps={formikProps}
                                />
                            </CardContent>
                            <CardActions className={styles.cardActions}>
                                {isModal && <Button color="primary" onClick={onClose}>Закрыть</Button>}
                                <Button
                                    type="submit"
                                    variant="contained"
                                    color="primary"
                                    disabled={isSubmitting || !isValid || loading}
                                    onClick={(e) => handleClickFormButton(e, submitForm)}
                                    {...buttonProps}
                                >
                                    {resolvedButtonText}
                                </Button>
                            </CardActions>
                        </Card>
                    </FormikForm>
                );
            }}
        </Formik>
    );
    
    return isModal ? (
        <Modal open={isOpen} onClose={onClose}>
            {renderForm()}
        </Modal>
    ) : renderForm();
};

export default Form;