import React from "react";
import Stepper from "./layout/Stepper";
import Grid from "./layout/Grid";
import Plain from "./layout/Plain";

const components = {
    stepper: Stepper,
    grid: Grid,
    plain: Plain,
};

const normalizeGridConfig = items => ({
    type: 'grid',
    options: {
        items,
    },
});

const normalizeLayout = layout =>
    layout.map(layoutItem => Array.isArray(layoutItem) ? normalizeGridConfig(layoutItem) : layoutItem);

const Layout = ({layout, renderField, formikProps}) => {
    const normalizedLayout = normalizeLayout(layout);

    return normalizedLayout.map(({type, options}, key) => {
        const Component = components[type];

        return (
            <Component
                key={key}
                options={options}
                renderField={renderField}
                formikProps={formikProps}
            />
        );
    });
};

export default Layout;

export const getUsedFields = layout => {
    const normalizedLayout = normalizeLayout(layout);

    const gridFields = normalizedLayout
        .filter(({type}) => type === 'grid')
        .map(({options: {items}}) => items)
        .reduce((all, current) => [...all, ...current], []);

    const stepperFields = normalizedLayout
        .filter(({type}) => type === 'stepper')
        .map(({options: {steps}}) => steps
            .map(({layout}) => getUsedFields(layout))
            .reduce((all, current) => [...all, ...current], []))
        .reduce((all, current) => [...all, ...current], []);

    return [...gridFields, ...stepperFields];
};