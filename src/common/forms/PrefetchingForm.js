import React, {useEffect, useState} from "react";
import Form from "./Form";
import useHttpApi from "../../hooks/useHttpApi";
import {omit, pick, isFunction} from "lodash";
import {mergeToFieldsWithKey} from "../../services/object";
import {useDispatch, useSelector} from "react-redux";
import {getPrefetchCacheById} from "../../store/reducers/rootReducer";
import {prefetchCacheAdd} from "../../store/actions/prefetchCache";

const EXPAND = ['_labels', '_validators'];

const PrefetchingForm = props => {
    const {makeRequest} = useHttpApi();
    const dispatch = useDispatch();
    const {
        prefetchOptions,
        usePrefetchCaching,
        prefetchCacheKey,
        cacheExtrasOnly,
        afterPrefetch,
        isModal,
        isOpen,
        loading: forceLoading,
    } = props;
    let {fields} = props;

    const cache = useSelector(getPrefetchCacheById(prefetchCacheKey));

    const [loading, setLoading] = useState(false);
    const [data, setData] = useState(usePrefetchCaching ? cache : null);
    const [firstFetch, setFirstFetch] = useState(true);


    // TODO Рефакторинг.

    useEffect(() => {
        if (forceLoading) {
            return;
        }

        const fetchData = async (onSuccess, fetchExtras = true) => {
            setLoading(true);

            const fetchOptions = fetchExtras
                ? {...prefetchOptions, expand: [...(prefetchOptions.expand || []), ...EXPAND]}
                : prefetchOptions;

            const response = await makeRequest(fetchOptions);
            response.status === 200 && onSuccess(response.data);

            setLoading(false);
        };

        if (usePrefetchCaching) {
            if (firstFetch) {
                if (!data) {
                    fetchData(data => {
                        const cacheData = cacheExtrasOnly ? pick(data, EXPAND) : data;

                        dispatch(prefetchCacheAdd(prefetchCacheKey, cacheData));
                        setData(data);

                        isFunction(afterPrefetch) && afterPrefetch(data);
                    });
                } else {
                    cacheExtrasOnly && fetchData(fetchedData => {
                        setData({...data, ...fetchedData});

                        isFunction(afterPrefetch) && afterPrefetch(data);
                    }, false);
                }
            }
        } else {
            if (isModal) {
                if (isOpen) {
                    setData(null);
                    fetchData(data => {
                        setData(data);

                        isFunction(afterPrefetch) && afterPrefetch(data);
                    });
                }
            } else {
                if (firstFetch) {
                    fetchData(data => {
                        setData(data);

                        isFunction(afterPrefetch) && afterPrefetch(data);
                    });
                }
            }
        }

        firstFetch && setFirstFetch(false);
    }, [prefetchOptions, isModal, isOpen]);

    if (data) {
        if (data._labels) {
            fields = mergeToFieldsWithKey(fields, data._labels, 'label');
        }
        if (data._validators) {
            fields = mergeToFieldsWithKey(fields, data._validators, 'validators');
        }
    }

    // TODO Разделить скелетоны лейблов и данных. Показывать лейблы, если этот флаг установлен.
    // const loadingExtras = !Boolean(data);

    return (
        <Form
            {...props}
            fields={fields}
            values={omit(data, ['_labels', '_validators'])}
            loading={loading || forceLoading}
        />
    );
};

export default PrefetchingForm;