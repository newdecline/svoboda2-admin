import React from "react";
import CloudOffIcon from "@material-ui/icons/CloudOff";
import Typography from "@material-ui/core/Typography";
import {makeStyles} from "@material-ui/core/styles/index";

const useStyles = makeStyles(theme => ({
    wrap: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
        paddingTop: theme.spacing(2),
        color: theme.palette.text.disabled,
    },
}));

const NoItemsPlaceholder = () => {
    const styles = useStyles();

    return (
        <div className={styles.wrap}>
            <CloudOffIcon style={{fontSize: 40}}/>
            <Typography>Не содержит элементов</Typography>
        </div>
    );
};

export default NoItemsPlaceholder;