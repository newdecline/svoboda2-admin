import React from "react";
import LinearProgress from "@material-ui/core/LinearProgress";

const CardProgress = ({loading}) => {

    const progressProps = loading ? {} : {
        variant: 'determinate',
        value: 100,
    };

    return <LinearProgress {...progressProps} />
};

export default CardProgress;