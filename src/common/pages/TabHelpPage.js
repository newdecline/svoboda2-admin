import React from "react";
import {Typography} from "@material-ui/core";
import Page from "./Page";

const TabHelpPage = ({header}) => {
    return (
        <Page header={header} divider={false}>
            <Typography variant="subtitle1" color="textSecondary" gutterBottom={true}>
                Воспользуйтесь левым меню, чтобы начать.
            </Typography>
        </Page>
    );
};

export default TabHelpPage;