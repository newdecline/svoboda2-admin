import React from "react";
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Container from '@material-ui/core/Container';
import Tooltip from '@material-ui/core/Tooltip';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import {makeStyles} from "@material-ui/core/index";
import {useHistory} from "react-router-dom";
import {useSelector} from "react-redux";
import {getFirstLoad} from "../../store/reducers/rootReducer";
import HelpIcon from "@material-ui/icons/HelpOutline";
import LaunchIcon from "@material-ui/icons/Launch";

const useStyles = makeStyles(theme => ({
    backButton: {
        marginRight: theme.spacing(1),
    },
    headerButtons: {
        marginLeft: theme.spacing(1),
    },
    headerWithIcons: {
        marginBottom: theme.spacing(1),
    },
    headerWithoutIcons: {
        marginBottom: theme.spacing(2),
    },
    hintTooltip: {
        fontSize: 12,
    },
    pageContent: {
        '& > *': {
            marginTop: theme.spacing(3),
            marginBottom: theme.spacing(3),
        },
    },
}));

const Page = ({header, children, backButton, divider, headerHint, headerLink}) => {
    const styles = useStyles();
    const history = useHistory();
    const firstLoad = useSelector(getFirstLoad);

    const headerHasIcons = (backButton && !firstLoad) || headerHint || headerLink;

    return (
        <Container>
            <Typography
                component="h1"
                variant="h4"
                className={headerHasIcons ? styles.headerWithIcons : styles.headerWithoutIcons}
            >
                {backButton && !firstLoad && (
                    <IconButton
                        className={styles.backButton}
                        title="Назад"
                        onClick={() => history.goBack()}
                    >
                        <ArrowBackIcon />
                    </IconButton>
                )}
                {header}
                <span className={styles.headerButtons}>
                    {headerHint && (
                        <Tooltip title={headerHint} classes={{tooltip: styles.hintTooltip}}>
                            <IconButton>
                                <HelpIcon />
                            </IconButton>
                        </Tooltip>
                    )}
                    {headerLink && (
                        <IconButton
                            title="Посмотреть на сайте"
                            href={headerLink}
                            target="_blank"
                        >
                            <LaunchIcon />
                        </IconButton>
                    )}
                </span>
            </Typography>
            {divider && <Divider />}
            <div className={styles.pageContent}>
                {children}
            </div>
        </Container>
    );
};

Page.defaultProps = {
    divider: true,
};

export default Page;