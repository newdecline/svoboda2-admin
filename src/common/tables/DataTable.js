import React, {useEffect, useState} from "react";
import useHttpApi from "../../hooks/useHttpApi";
import {entries, keys, mapKeys, mapValues, pickBy, range} from "lodash";
import {build, mergeToFieldsWithKey} from "../../services/object";
import Pagination from "./Pagination";
import {useHistory} from "react-router-dom";
import MuiTable from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import IconButton from '@material-ui/core/IconButton';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import LinearProgress from '@material-ui/core/LinearProgress';
import {makeStyles} from "@material-ui/core/styles";
import Skeleton from "@material-ui/lab/Skeleton";
import EditIcon from "@material-ui/icons/Edit";
import CheckBoxIcon from "@material-ui/icons/CheckBox";
import CheckBoxOutlineBlankIcon from "@material-ui/icons/CheckBoxOutlineBlank";
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import {
    Card,
    CardContent,
    CardActions,
} from "@material-ui/core";
import CardHeader from "../CardHeader";
import CardProgress from "../CardProgress";
import DeleteIcon from "@material-ui/icons/Delete";
import ConfirmModal from "../modals/ConfirmModal";

const useStyles = makeStyles(theme => ({
    paper: {
        width: '100%',
        overflowX: 'auto',
        padding: 0,
    },
    paperHeader: {
        marginTop: theme.spacing(2),
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
    },
    cellSkeleton: {
        maxWidth: 150,
        margin: 0,
    },
    cardActions: {
        justifyContent: 'flex-end',
    },
    cellWithIconInner: {
        display: 'flex',
        alignItems: 'center',
    },
    cellIcon: {
        display: 'flex',
        padding: theme.spacing(1),
    },
    hiddenItemRow: {
        opacity: .5,
    },
    tableWrap: {
        padding: '0 !important',
    },
}));

const SKELETON_ROWS_COUNT = 5;
const DEFAULT_DATA_TYPE = 'string';
const DEFAULT_BOOLEAN_RENDER = 'string';
const DEFAULT_SORTING_ORDER = 'desc';
const ANOTHER_SORTING_ORDER = DEFAULT_SORTING_ORDER === 'desc' ? 'asc' : 'desc';
const DEFAULT_PAGE = 1;
const DEFAULT_PER_PAGE = 10;
const DEFAULT_PER_PAGE_OPTIONS = [.5, 1, 2.5, 5].map(factor => DEFAULT_PER_PAGE * factor);

const ACTIONS = {
    'edit': {
        icon: <EditIcon />,
        title: 'Редактировать',
    },
    'moveUp': {
        icon: <ArrowUpwardIcon />,
        title: 'Переместить вверх',
    },
    'moveDown': {
        icon: <ArrowDownwardIcon />,
        title: 'Переместить вниз',
    },
    'delete': {
        icon: <DeleteIcon />,
        title: 'Удалить',
    },
};

const BOOLEAN_RENDERS = {
    'checkbox': {
        renderValue: value => value
            ? <CheckBoxIcon color="primary" />
            : <CheckBoxOutlineBlankIcon color="primary" />,
        valueTitle: value => value ? "Да" : "Нет",
    },
    'string': {
        renderValue: value => value ? 'Да' : 'Нет',
        valueTitle: () => null,
    },
};

const DATA_TYPES = {
    string: {
        align: 'left',
        renderValue: value => value,
        valueTitle: () => null,
    },
    boolean: {
        align: 'center',
        renderValue: (value, options={}) => BOOLEAN_RENDERS[options.renderAs || DEFAULT_BOOLEAN_RENDER].renderValue(value),
        valueTitle: (value, options={}) => BOOLEAN_RENDERS[options.renderAs || DEFAULT_BOOLEAN_RENDER].valueTitle(value),
    },
};

const DataTable = props => {
    const {
        header,
        fetchOptions,
        columns,
        updateItemForm: UpdateItemForm,
        createItemForm: CreateItemForm,
        updateItemRoute,
        polymorphic,
        sorting,
        deleteItemOptions,
    } = props;

    const styles = useStyles();
    const {makeRequest} = useHttpApi();
    const history = useHistory();

    const [globalLoading, setGlobalLoading] = useState(true);
    const [data, setData] = useState(null);
    const [itemToEdit, setItemToEdit] = useState(null);
    const [currentPage, setCurrentPage] = useState(1);
    const [createItemFormIsOpen, setCreateItemFormIsOpen] = useState(false);
    const [sortBy, setSortBy] = useState(null);
    const [sortingOrder, setSortingOrder] = useState(DEFAULT_SORTING_ORDER);
    const [perPage, setPerPage] = useState(DEFAULT_PER_PAGE);
    const [loading, setLoading] = useState(false);
    const [itemToDelete, setItemToDelete] = useState(null);

    const columnsWithMeta = !polymorphic ? {
        ...columns,
        ...{
            id: {hidden: true},
            is_hidden: {hidden: true},
            type: {hidden: true},
        },
    } : {
        ...columns,
        ...mapValues(mapKeys(columns, (value, key) => `model.${key}`), () => ({hidden: true})),
        id: {hidden: true},
        model_type: {hidden: true},
    };

    const renderHeadCell = (key, columnConfig) => {
        const {label, type} = columnConfig;
        const {align} = DATA_TYPES[type];
        const active = key === sortBy;

        return (
            <TableCell
                key={key}
                align={align}
            >
                {sorting ? (
                    <TableSortLabel
                        active={active}
                        direction={active ? sortingOrder : DEFAULT_SORTING_ORDER}
                        onClick={() => handleSortLabelClick(key)}
                    >
                        {label}
                    </TableSortLabel>
                ) : (
                    label
                )}
            </TableCell>
        );
    };
    const renderBodyCell = (key, columnConfig, value, isHidden=false) => {
        const {type, options} = columnConfig;
        const {align, valueTitle, renderValue} = DATA_TYPES[type];

        return (
            <TableCell
                key={key}
                align={align}
                title={valueTitle(value, options)}
            >
                {options && options.includeVisibilityIcon && isHidden ? (
                    <div className={styles.cellWithIconInner}>
                        {renderValue(value, options)}
                        <div className={styles.cellIcon} title="Элемент не выводится на сайте">
                            <VisibilityOffIcon fontSize="small" />
                        </div>
                    </div>
                ) : (
                    renderValue(value, options)
                )}
            </TableCell>
        );
    };

    const fetchPage = async (page, perPage, sortBy, sortingOrder, firstTime = false) => {
        firstTime ? setGlobalLoading(true) : setLoading(true);

        const expand = !polymorphic ? ['_tableMeta'] : ['_tableMeta', 'model'];

        let query = fetchOptions.query || {};

        query.page = page;
        query['per-page'] = perPage;

        sorting && sortBy && (query.sort = `${sortingOrder === 'asc' ? '-' : ''}${sortBy}`);

        const response = await makeRequest({
            ...fetchOptions,
            ...{
                fields: keys(columnsWithMeta),
                expand: expand,
                query,
            }
        });
        if (response.status === 200) {
            setData(response.data);
            setCurrentPage(response.data._tableMeta.pagination.currentPage);
            setPerPage(response.data._tableMeta.pagination.perPage);
        }

        firstTime ? setGlobalLoading(false) : setLoading(false);
    };

    useEffect(() => {
        const fetchData = async () => {
            await fetchPage(DEFAULT_PAGE, perPage, sortBy, sortingOrder, true);
        };

        fetchData();
    }, []);

    const redirectToUpdatePage = ({id}) => history.push(`${updateItemRoute}?id=${id}`);

    const handleUpdateSuccess = async () => {
        setItemToEdit(null);
        await fetchPage(currentPage, perPage, sortBy, sortingOrder);
    };

    const handleCreateSuccess = async data => {
        if (updateItemRoute) {
            redirectToUpdatePage(data);
        } else {
            setCreateItemFormIsOpen(false);
            await fetchPage(DEFAULT_PAGE, perPage, null, DEFAULT_SORTING_ORDER);
        }
    };

    const handleChangePage = async page =>
        await fetchPage(page, perPage, sortBy, sortingOrder);

    const handleChangeRowsPerPage = async event =>
        await fetchPage(DEFAULT_PAGE, parseInt(event.target.value, 10), sortBy, sortingOrder);

    const makeRequestAndReload = async options => {
        await makeRequest(options);
        await fetchPage(currentPage, perPage, sortBy, sortingOrder);
    };

    const getActions = ({updateItemForm, updateItemRoute, moveUpOptions, moveDownOptions, deleteItemOptions}) => {
        let actions = {};
        if (updateItemForm) {
            actions = {...actions, ...{
                edit: item => setItemToEdit(item),
            }};
        }
        if (updateItemRoute) {
            actions = {...actions, ...{
                edit: redirectToUpdatePage,
            }};
        }
        if (moveUpOptions) {
            actions = {...actions, ...{
                moveUp: item => makeRequestAndReload(moveUpOptions(item)),
            }};
        }
        if (moveDownOptions) {
            actions = {...actions, ...{
                moveDown: item => makeRequestAndReload(moveDownOptions(item)),
            }};
        }
        if (deleteItemOptions) {
            actions = {...actions, ...{
                delete: item => setItemToDelete(item),
            }};
        }

        return actions;
    };

    const actions = getActions(props);
    const actionIsUsed = keys(actions).length > 0;

    const columnsWithLabels = data
        ? mergeToFieldsWithKey(columnsWithMeta, data._tableMeta.labels, 'label', false)
        : columnsWithMeta;
    const columnsConfig = build(columnsWithLabels, config => ({...config, type: config.type || DEFAULT_DATA_TYPE}));
    const shownColumns = pickBy(columnsConfig, config => !config.hidden);

    const handleSortLabelClick = async field => {
        let newSortBy = sortBy, newSortingOrder = sortingOrder;

        if (field !== sortBy) {
            newSortBy = field;
            newSortingOrder = DEFAULT_SORTING_ORDER;
        } else {
            if (sortingOrder === DEFAULT_SORTING_ORDER) {
                newSortingOrder = ANOTHER_SORTING_ORDER;
            } else {
                newSortBy = null;
            }
        }

        setSortBy(newSortBy);
        setSortingOrder(newSortingOrder);

        await fetchPage(DEFAULT_PAGE, perPage, newSortBy, newSortingOrder);
    };

    const renderNoDataPlaceholder = () =>
        <TableRow>
            <TableCell colSpan={keys(shownColumns).length + (actionIsUsed ? 1 : 0)}>
                Нет данных
            </TableCell>
        </TableRow>;

    return (
        <>
            <Card>
                {header && (
                    <CardContent>
                        <CardHeader
                            header={header}
                            addAction={CreateItemForm && (() => setCreateItemFormIsOpen(true))}
                        />
                    </CardContent>
                )}
                <CardProgress loading={loading} />
                <CardContent className={styles.tableWrap}>
                    <MuiTable>
                        {!globalLoading &&
                            <TableHead>
                                <TableRow>
                                    {entries(shownColumns).map(([key, columnConfig]) => renderHeadCell(key, columnConfig))}
                                    {actionIsUsed && <TableCell />}
                                </TableRow>
                            </TableHead>
                        }
                        <TableBody>
                            {!globalLoading ? (data.items.length
                                    ? data.items.map((item, key) => {
                                            const model = !polymorphic ? item : item.model;

                                            return (
                                                <TableRow key={key} className={model.is_hidden ? styles.hiddenItemRow : null}>
                                                    {entries(shownColumns).map(([key, columnConfig]) => renderBodyCell(key, columnConfig, model[key], model.is_hidden))}
                                                    {actionIsUsed &&
                                                    <TableCell align="right">
                                                        {keys(actions).map(key =>
                                                            <IconButton
                                                                key={key}
                                                                onClick={() => actions[key](item)}
                                                                title={ACTIONS[key].title}
                                                            >
                                                                {ACTIONS[key].icon}
                                                            </IconButton>
                                                        )}
                                                    </TableCell>
                                                    }
                                                </TableRow>
                                            );
                                        }
                                    ) : renderNoDataPlaceholder()
                            ) : (
                                range(SKELETON_ROWS_COUNT).map(key =>
                                    <TableRow key={key}>
                                        {keys(shownColumns).map(key =>
                                            <TableCell key={key}>
                                                <Skeleton className={styles.cellSkeleton} />
                                            </TableCell>
                                        )}
                                        {actionIsUsed &&
                                        <TableCell>
                                            <Skeleton className={styles.cellSkeleton} />
                                        </TableCell>
                                        }
                                    </TableRow>
                                )
                            )}
                        </TableBody>
                    </MuiTable>
                </CardContent>
                {!globalLoading &&
                    <CardActions className={styles.cardActions}>
                        <Pagination
                            data={data}
                            onChangePage={handleChangePage}
                            rowsPerPageOptions={DEFAULT_PER_PAGE_OPTIONS}
                            onChangeRowsPerPage={handleChangeRowsPerPage}
                        />
                    </CardActions>
                }
            </Card>
            {itemToEdit && (!polymorphic ? (
                <UpdateItemForm
                    isModal
                    isOpen={Boolean(itemToEdit)}
                    onClose={() => setItemToEdit(null)}
                    onSuccess={handleUpdateSuccess}
                    id={itemToEdit.id}
                    type={itemToEdit.type}
                />
            ) : (
                <UpdateItemForm
                    isModal
                    isOpen={Boolean(itemToEdit)}
                    onClose={() => setItemToEdit(null)}
                    onSuccess={handleUpdateSuccess}
                    index={itemToEdit}
                />
            ))
            }
            {CreateItemForm &&
                <CreateItemForm
                    isModal
                    isOpen={createItemFormIsOpen}
                    onClose={() => setCreateItemFormIsOpen(false)}
                    onSuccess={handleCreateSuccess}
                />
            }
            {itemToDelete && (
                <ConfirmModal
                    message="Вы уверены, что хотите удалить элемент?"
                    isOpen={Boolean(itemToDelete)}
                    onCancel={() => setItemToDelete(null)}
                    onConfirm={() => {
                        makeRequestAndReload(deleteItemOptions(itemToDelete));
                        setItemToDelete(null);
                    }}
                    confirmButtonText="Удалить"
                    confirmButtonColor="secondary"
                />
            )}
        </>
    );
};

export default DataTable;