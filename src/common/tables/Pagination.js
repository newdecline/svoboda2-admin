import React from "react";
import TablePagination from '@material-ui/core/TablePagination';

const Pagination = props => {
    const {
        data,
        onChangePage,
    } = props;

    return (
        <TablePagination
            component="div"
            labelDisplayedRows={({from, to, count}) => `${from}-${to} из ${count}`}
            labelRowsPerPage="Выводить по:"
            rowsPerPage={data._tableMeta.pagination.perPage}
            rowsPerPageOptions={[data._tableMeta.pagination.perPage]}
            page={data._tableMeta.pagination.currentPage - 1}
            count={data._tableMeta.pagination.totalCount}
            {...props}
            onChangePage={(event, page) => onChangePage(page + 1)}
        />
    );
};

export default Pagination;