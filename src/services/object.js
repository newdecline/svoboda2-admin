import {keys} from "lodash";

export const build = (sourceObject, callback) => keys(sourceObject)
    .map(key => [key, callback(sourceObject[key], key)])
    .reduce((buildObject, [key, value]) => ({...buildObject, [key]: value}), {});

/**
 * Возвращает объект, построенный на основе mergeToObject.
 * Значения объекта mergeFromObject записываются в "дочерние" объекты mergeToObject под ключем key.
 *
 * @example
 * // Возвращает
 * // {
 * //     email: {
 * //         type: 'input',
 * //         label: 'Email',
 * //     },
 * //     password: {
 * //         type: 'input',
 * //         label: 'Пароль',
 * //     },
 * //     rememberMe: {
 * //         type: 'checkbox',
 * //         label: 'Запомнить меня',
 * //     },
 * // }
 * mergeToFieldsWithKey({
 *     email: {
 *         type: 'input',
 *         label: 'Email',
 *     },
 *     password: {
 *         type: 'input',
 *     },
 *     rememberMe: {
 *         type: 'checkbox',
 *     },
 * }, {
 *     login: 'Логин',
 *     password: 'Пароль',
 *     rememberMe: 'Запомнить меня',
 * }, 'label');
 *
 * @param {object} mergeToObject В поля какого объекта выполнять слияние.
 * @param {object} mergeFromObject Из какого объекта брать значения.
 * @param {string} key Ключ, по которому записать значения mergeFromObject.
 * @param {boolean} replace Заменять значения, если они уже установлены.
 *
 * @returns {object} Результат слияния объектов.
 */
export const mergeToFieldsWithKey = (mergeToObject, mergeFromObject, key, replace = true) => {
    const mergeWithReplace = (mergeToObjectValue, mergeToObjectKey) =>
        ({...mergeToObjectValue, [key]: mergeFromObject[mergeToObjectKey]});

    const resolveReplace = (mergeToObjectValue, mergeToObjectKey) => {
        if (replace) {
            return mergeWithReplace(mergeToObjectValue, mergeToObjectKey);
        } else {
            return mergeToObjectValue[key] ? mergeToObjectValue : mergeWithReplace(mergeToObjectValue, mergeToObjectKey);
        }
    };

    const buildValue = (mergeToObjectValue, mergeToObjectKey) =>
        mergeFromObject[mergeToObjectKey] ? resolveReplace(mergeToObjectValue, mergeToObjectKey) : mergeToObjectValue;

    return build(mergeToObject, buildValue);
};