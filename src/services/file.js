const getExtensionDotIndex = fileName => (fileName.lastIndexOf(".") - 1 >>> 0) + 1;

export const getFileExtension = file =>
    file.name
        .slice(getExtensionDotIndex(file.name) + 1)
        .toLowerCase();

export const getFileName = file =>
    file.name.slice(0, getExtensionDotIndex(file.name));

export const createImageFromUrl = url => new Promise(resolve => {
    let image = new Image();
    image.onload = () => resolve(image);
    image.onerror = () => resolve({width: 0, height: 0});
    image.src = url;
});