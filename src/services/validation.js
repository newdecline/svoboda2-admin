import * as Yup from "yup";
import {build} from "./object";
import moment from "moment";
import {createImageFromUrl} from "./file";

const converters = {
    filter_validator: (yup, options) => {
        let _yup = yup;

        options.length === 0 && (_yup = _yup.trim());

        return _yup;
    },
    string_validator: (yup, options) => {
        let _yup = yup;

        options.max && (_yup = _yup.max(options.max, options.tooLong));
        options.min && (_yup = _yup.min(options.min, options.tooShort));

        return _yup;
    },
    email_validator: (yup, options) => {
        let _yup = yup;

        _yup = _yup.email(options.message);

        return _yup;
    },
    url_validator: (yup, options) => {
        let _yup = yup;

        _yup = _yup.url(options.message);

        return _yup;
    },
    required_validator: (yup, options) => {
        let _yup = yup;

        _yup = _yup.required(options.message);

        return _yup;
    },
    date_validator: (yup, options) => {
        let _yup = yup;

        options.format && (_yup = _yup.test(
            'format',
            options.message,
            value => moment(value, options.format, true).isValid() || !value,
        ));

        return _yup;
    },
    file_id_validator: (yup, options) => {
        let _yup = yup;

        const {extensions, maxSize, wrongExtension, tooBig} = options;
        
        extensions && (_yup = _yup.test(
            'extension',
            wrongExtension,
            value => !value || extensions.includes(value.extension),
        ));

        maxSize && (_yup = _yup.test(
            'maxSize',
            tooBig,
            value => !value || value.size <= maxSize,
        ));

        return _yup;
    },
    image_id_validator: (yup, options) => {
        let _yup = yup;

        const {
            minWidth,
            minHeight,
            maxWidth,
            maxHeight,
            notImage,
            underWidth,
            overWidth,
            underHeight,
            overHeight,
        } = options;

        _yup = _yup.test(
            'notImage',
            notImage.replace(" «{file}» ", " "),
            async value => {
                if (!value) return true;

                const image = await createImageFromUrl(value.url);

                return image.width > 0 && image.height > 0;
            },
        );

        minWidth && (_yup = _yup.test(
            'underWidth',
            underWidth.replace(" «{file}» ", " "),
            async value => {
                if (!value) return true;

                const image = await createImageFromUrl(value.url);
                
                return image.width >= minWidth;
            },
        ));

        minHeight && (_yup = _yup.test(
            'underHeight',
            underHeight.replace(" «{file}» ", " "),
            async value => {
                if (!value) return true;

                const image = await createImageFromUrl(value.url);

                return image.height >= minHeight;
            },
        ));

        maxWidth && (_yup = _yup.test(
            'overWidth',
            overWidth.replace(" «{file}» ", " "),
            async value => {
                if (!value) return true;

                const image = await createImageFromUrl(value.url);

                return image.width <= maxWidth;
            },
        ));

        maxHeight && (_yup = _yup.test(
            'overHeight',
            overHeight.replace(" «{file}» ", " "),
            async value => {
                if (!value) return true;

                const image = await createImageFromUrl(value.url);

                return image.height <= maxHeight;
            },
        ));

        return _yup;
    },

    // Без реализации
    boolean_validator: yup => yup,
    number_validator: yup => yup,
    inline_validator: yup => yup,
    each_validator: yup => yup,
};

const defineSchema = usedValidators => {
    if (usedValidators.includes('boolean_validator')) {
        return Yup.boolean();
    }
    if (usedValidators.includes('file_id_validator') || usedValidators.includes('image_id_validator')) {
        return Yup.object().nullable();
    }

    // String schema as default
    return Yup.string().nullable();
};

const createYupValidators = yiiValidators => {
    const usedValidators = yiiValidators.map(yiiValidator => yiiValidator.validator);

    let validators = defineSchema(usedValidators);
    
    yiiValidators.map(yiiValidator => validators = converters[yiiValidator.validator](validators, yiiValidator.options, usedValidators));

    return validators;
};

export const createValidationSchema = fields => {
    const shape = build(fields, fieldConfig => fieldConfig.validators.length && createYupValidators(fieldConfig.validators));
    
    return Yup.object().shape(shape);
};