const BASE_ROUTE = '/companies-page';
const COMMON_CONFIG = {
    expand: ['bannerImageOriginal', 'bannerImage'],
};

export const getCompaniesPage = () => ({
    route: BASE_ROUTE,
    ...COMMON_CONFIG,
});

export const updateCompaniesPage = data => ({
    route: BASE_ROUTE,
    method: 'put',
    body: data,
    ...COMMON_CONFIG,
});