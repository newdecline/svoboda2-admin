const BASE_ROUTE = '/rent-page';
const COMMON_CONFIG = {
    expand: ['bannerImageOriginal', 'bannerImage'],
};

export const getRentPage = () => ({
    route: BASE_ROUTE,
    ...COMMON_CONFIG,
});

export const updateRentPage = data => ({
    route: BASE_ROUTE,
    method: 'put',
    body: data,
    ...COMMON_CONFIG,
});