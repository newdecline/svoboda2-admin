const BASE_ROUTE = '/profile';
const COMMON_FIELDS = [
    '*',
    'avatarImage.id',
    'avatarImage.url',
    'avatarImage.alt',
];

export const getProfile = () => ({
    route: BASE_ROUTE,
    fields: COMMON_FIELDS,
});

export const updateProfile = data => ({
    route: BASE_ROUTE,
    method: 'put',
    body: data,
    fields: COMMON_FIELDS,
});