const BASE_ROUTE = '/page-metas';

export const getPageMeta = id => ({
    route: `${BASE_ROUTE}/${id}`,
});

export const updatePageMeta = (id, data) => ({
    route: `${BASE_ROUTE}/${id}`,
    method: 'put',
    body: data,
});

export const listPageMetas = () => ({
    route: BASE_ROUTE,
});