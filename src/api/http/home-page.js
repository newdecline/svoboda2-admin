const BASE_ROUTE = '/home-page';
const COMMON_CONFIG = {
    expand: ['ownEventOriginalImage', 'ownEventImage', 'companiesOriginalImage', 'companiesImage'],
};

export const getHomePage = () => ({
    route: BASE_ROUTE,
    ...COMMON_CONFIG,
});

export const updateHomePage = data => ({
    route: BASE_ROUTE,
    method: 'put',
    body: data,
    ...COMMON_CONFIG,
});