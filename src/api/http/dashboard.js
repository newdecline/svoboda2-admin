const BASE_ROUTE = '/dashboard';

export const getDashboard = (authToken = null) => ({
    route: BASE_ROUTE,
    authToken: authToken,
});