const BASE_ROUTE = '/users';

export const listUsers = () => ({
    route: BASE_ROUTE,
});