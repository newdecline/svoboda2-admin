const BASE_ROUTE = '/images';

export const getImage = id => ({
    route: `${BASE_ROUTE}/${id}`,
});

export const updateImage = (id, data) => ({
    route: `${BASE_ROUTE}/${id}`,
    method: 'put',
    body: data,
});