import {NEXT_DIRECTION, PREV_DIRECTION} from "../constants";

const BASE_ROUTE = '/events';
const COMMON_CONFIG = {
    expand: [
        'originalImage',
        'bannerImage',
        'thumbnailImage',
        'gallery',
        'pageMeta',
    ],
};

export const listEvents = () => ({
    route: BASE_ROUTE,
});

export const getEvent = id => ({
    route: `${BASE_ROUTE}/${id}`,
    ...COMMON_CONFIG,
});

export const getCreateEventForm = () => ({
    route: `${BASE_ROUTE}/create-form`,
});

export const createEvent = data => ({
    route: BASE_ROUTE,
    method: 'post',
    body: data,
    ...COMMON_CONFIG,
});

export const updateEvent = (id, data) => ({
    route: `${BASE_ROUTE}/${id}`,
    method: 'put',
    body: data,
    ...COMMON_CONFIG,
});

export const deleteEvent = id => ({
    route: `${BASE_ROUTE}/${id}`,
    method: 'delete',
});

export const moveEvent = (id, data) => ({
    route: `${BASE_ROUTE}/${id}/move`,
    method: 'put',
    body: data,
    snackbar: false,
});

export const moveNextEvent = id => moveEvent(id, {direction: PREV_DIRECTION});
export const movePrevEvent = id => moveEvent(id, {direction: NEXT_DIRECTION});