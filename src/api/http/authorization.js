const BASE_ROUTE = '/authorization';

export const getLoginForm = () => ({
    route: `${BASE_ROUTE}/login-form`,
});

export const login = data => ({
    route: `${BASE_ROUTE}/login`,
    method: 'post',
    body: data,
    snackbar: false,
});

export const logout = () => ({
    route: `${BASE_ROUTE}/logout`,
    method: 'post',
    snackbar: false,
});