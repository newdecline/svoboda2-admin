import {NEXT_DIRECTION, PREV_DIRECTION} from "../constants";

const BASE_ROUTE = '/rooms';
const COMMON_CONFIG = {
    expand: [
        'originalImage',
        'bannerImage',
        'thumbnailImage',
        'gallery',
        'pageMeta',
    ],
};

export const listRooms = () => ({
    route: BASE_ROUTE,
    query: {
        sort: 'position',
    },
});

export const getRoom = id => ({
    route: `${BASE_ROUTE}/${id}`,
    ...COMMON_CONFIG,
});

export const getCreateRoomForm = () => ({
    route: `${BASE_ROUTE}/create-form`,
});

export const createRoom = data => ({
    route: BASE_ROUTE,
    method: 'post',
    body: data,
    ...COMMON_CONFIG,
});

export const updateRoom = (id, data) => ({
    route: `${BASE_ROUTE}/${id}`,
    method: 'put',
    body: data,
    ...COMMON_CONFIG,
});

export const deleteRoom = id => ({
    route: `${BASE_ROUTE}/${id}`,
    method: 'delete',
});

export const moveRoom = (id, data) => ({
    route: `${BASE_ROUTE}/${id}/move`,
    method: 'put',
    body: data,
    snackbar: false,
});

export const moveNextRoom = id => moveRoom(id, {direction: PREV_DIRECTION});
export const movePrevRoom = id => moveRoom(id, {direction: NEXT_DIRECTION});