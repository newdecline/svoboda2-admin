const BASE_ROUTE = '/static-files';
const ROBOTS_ID = 'robots';
const SITEMAP_ID = 'sitemap';

const getStaticFile = id => ({
    route: `${BASE_ROUTE}/${id}`,
});

const updateStaticFile = (id, data) => ({
    route: `${BASE_ROUTE}/${id}`,
    method: 'put',
    body: data,
});

export const getRobots = () => getStaticFile(ROBOTS_ID);
export const updateRobots = data => updateStaticFile(ROBOTS_ID, data);

export const getSitemap = () => getStaticFile(SITEMAP_ID);
export const updateSitemap = data => updateStaticFile(SITEMAP_ID, data);