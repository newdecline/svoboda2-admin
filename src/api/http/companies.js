import {NEXT_DIRECTION, PREV_DIRECTION} from "../constants";

const BASE_ROUTE = '/companies';
const COMMON_CONFIG = {
    expand: [
        'name',
        'vk_url',
        'instagram_url',
        'facebook_url',
        'site_url',
        'description',
        'address',
        'email',
        'renter_from',

        'bannerImage',
        'avatarImage',
        'locationImage',

        'pageMeta',
        'gallery',

        'originalBannerImage',
        'originalAvatarImage',

        'avatarAlt',
        'bannerAlt',
        'locationImageAlt',
        'tagIds',
    ],
};

export const listCompanies = () => ({
    route: BASE_ROUTE,
});

export const getCompany = id => ({
    route: `${BASE_ROUTE}/${id}`,
    ...COMMON_CONFIG,
});

export const getCreateCompanyForm = () => ({
    route: `${BASE_ROUTE}/create-form`,
});

export const createCompany = data => ({
    route: BASE_ROUTE,
    method: 'post',
    body: data,
    ...COMMON_CONFIG,
});

export const updateCompany = (id, data) => ({
    route: `${BASE_ROUTE}/${id}`,
    method: 'put',
    body: data,
    ...COMMON_CONFIG,
});

export const deleteCompany = id => ({
    route: `${BASE_ROUTE}/${id}`,
    method: 'delete',
});

export const moveCompany = (id, data) => ({
    route: `${BASE_ROUTE}/${id}/move`,
    method: 'put',
    body: data,
    snackbar: false,
});

export const moveNextCompany = id => moveCompany(id, {direction: PREV_DIRECTION});
export const movePrevCompany = id => moveCompany(id, {direction: NEXT_DIRECTION});