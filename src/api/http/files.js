const BASE_ROUTE = '/files';

export const createFile = file => {
    let formData = new FormData();
    formData.append('file', file);

    return {
        route: BASE_ROUTE,
        method: 'post',
        body: formData,
        contentType: false,
        snackbar: false,
    };
};

export const cropFile = (id, data) => ({
    route: `${BASE_ROUTE}/${id}/crop`,
    method: 'post',
    body: data,
    snackbar: false,
});