const BASE_ROUTE = '/layout';

export const getLayout = () => ({
    route: BASE_ROUTE,
});

export const updateLayout = data => ({
    route: BASE_ROUTE,
    method: 'put',
    body: data,
});