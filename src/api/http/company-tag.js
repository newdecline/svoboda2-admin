import {NEXT_DIRECTION, PREV_DIRECTION} from "../constants";

const BASE_ROUTE = '/company-tags';
const COMMON_CONFIG = {};

export const listCompanyTags = () => ({
    route: BASE_ROUTE,
    query: {
        sort: 'position',
    },
});

export const getCompanyTag = id => ({
    route: `${BASE_ROUTE}/${id}`,
    ...COMMON_CONFIG,
});

export const getCreateCompanyTagForm = () => ({
    route: `${BASE_ROUTE}/create-form`,
});

export const createCompanyTag = data => ({
    route: BASE_ROUTE,
    method: 'post',
    body: data,
    ...COMMON_CONFIG,
});

export const updateCompanyTag = (id, data) => ({
    route: `${BASE_ROUTE}/${id}`,
    method: 'put',
    body: data,
    ...COMMON_CONFIG,
});

export const deleteCompanyTag = id => ({
    route: `${BASE_ROUTE}/${id}`,
    method: 'delete',
});

export const moveCompanyTag = (id, data) => ({
    route: `${BASE_ROUTE}/${id}/move`,
    method: 'put',
    body: data,
    snackbar: false,
});

export const moveNextCompanyTag = id => moveCompanyTag(id, {direction: PREV_DIRECTION});
export const movePrevCompanyTag = id => moveCompanyTag(id, {direction: NEXT_DIRECTION});

export const listAllowedToSet = () => ({
    route: `${BASE_ROUTE}/allowed-to-set`,
});