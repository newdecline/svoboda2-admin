const BASE_ROUTE = '/feedback-forms';

export const getFeedbackForm = id => ({
    route: `${BASE_ROUTE}/${id}`,
});

export const updateFeedbackForm = (id, data) => ({
    route: `${BASE_ROUTE}/${id}`,
    method: 'put',
    body: data,
});

export const listFeedbackForms = () => ({
    route: BASE_ROUTE,
});