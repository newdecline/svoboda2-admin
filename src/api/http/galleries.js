import {NEXT_DIRECTION, PREV_DIRECTION} from "../constants";

const BASE_ROUTE = '/galleries';
const SLIDES_ROUTE = '/gallery-slides';

export const listGalleries = () => ({
    route: BASE_ROUTE,
});

export const getGallery = galleryId => ({
    route: `${BASE_ROUTE}/${galleryId}`,
});

export const getCreateGallerySlideForm = galleryId => ({
    route: `${BASE_ROUTE}/${galleryId}/create-gallery-slide-form`,
});

export const listGallerySlides = galleryId => ({
    route: `${BASE_ROUTE}/${galleryId}${SLIDES_ROUTE}`,
});

export const createGallerySlide = (galleryId, data) => ({
    route: `${BASE_ROUTE}/${galleryId}${SLIDES_ROUTE}`,
    method: 'post',
    body: data,
});

export const getGallerySlide = slideId => ({
    route: `${SLIDES_ROUTE}/${slideId}`,
});

export const updateGallerySlide = (slideId, data, options = {}) => ({
    ...options,
    route: `${SLIDES_ROUTE}/${slideId}`,
    method: 'put',
    body: data,
});

export const deleteGallerySlide = slideId => ({
    route: `${SLIDES_ROUTE}/${slideId}`,
    method: 'delete',
    snackbar: false,
});

export const moveGallerySlide = (slideId, data) => ({
    route: `${SLIDES_ROUTE}/${slideId}/move`,
    method: 'put',
    body: data,
    snackbar: false,
});

export const moveNextGallerySlide = slideId => moveGallerySlide(slideId, {direction: PREV_DIRECTION});
export const movePrevGallerySlide = slideId => moveGallerySlide(slideId, {direction: NEXT_DIRECTION});

export const updateGallerySlideVisibility = (slideId, isHidden) => ({
    route: `${SLIDES_ROUTE}/${slideId}/update-visibility`,
    method: 'put',
    body: {isHidden},
    snackbar: false,
});