const BASE_ROUTE = '/events-page';
const COMMON_CONFIG = {
    expand: ['bannerImageOriginal', 'bannerImage'],
};

export const getEventsPage = () => ({
    route: BASE_ROUTE,
    ...COMMON_CONFIG,
});

export const updateEventsPage = data => ({
    route: BASE_ROUTE,
    method: 'put',
    body: data,
    ...COMMON_CONFIG,
});