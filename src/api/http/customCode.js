const BASE_ROUTE = '/custom-code';

export const getCustomCode = () => ({
    route: BASE_ROUTE,
});

export const updateCustomCode = data => ({
    route: BASE_ROUTE,
    method: 'put',
    body: data,
});