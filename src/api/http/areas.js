import {NEXT_DIRECTION, PREV_DIRECTION} from "../constants";

const BASE_ROUTE = '/areas';

export const listAreas = () => ({
    route: BASE_ROUTE,
    query: {
        sort: 'position',
    },
});

export const getArea = id => ({
    route: `${BASE_ROUTE}/${id}`,
});

export const getCreateAreaForm = () => ({
    route: `${BASE_ROUTE}/create-form`,
});

export const createArea = data => ({
    route: BASE_ROUTE,
    method: 'post',
    body: data,
});

export const updateArea = (id, data) => ({
    route: `${BASE_ROUTE}/${id}`,
    method: 'put',
    body: data,
});

export const deleteArea = id => ({
    route: `${BASE_ROUTE}/${id}`,
    method: 'delete',
});

export const moveArea = (id, data) => ({
    route: `${BASE_ROUTE}/${id}/move`,
    method: 'put',
    body: data,
    snackbar: false,
});

export const moveNextArea = id => moveArea(id, {direction: PREV_DIRECTION});
export const movePrevArea = id => moveArea(id, {direction: NEXT_DIRECTION});