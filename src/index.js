import React from 'react';
import {Provider} from "react-redux";
import ReactDOM from 'react-dom';
import './index.css';
import 'cropperjs/dist/cropper.css';
import App from './App';
import store from "./store";
import {SnackbarProvider} from 'notistack';
import CssBaseline from '@material-ui/core/CssBaseline';
import {MuiPickersUtilsProvider} from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import moment from "moment";
import "moment/locale/ru";
import {MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';

document.title = window.location.hostname + ' | Администрирование';

moment.locale("ru");

const theme = createMuiTheme({
    palette: {
        primary: {main: '#B41836'},
        secondary: {main: '#000000'},
        error: {main: '#B41836'},
    },
});

ReactDOM.render(
    <Provider store={store}>
        <MuiThemeProvider theme={theme}>
            <MuiPickersUtilsProvider utils={MomentUtils}>
                <SnackbarProvider maxSnack={3}>
                    <CssBaseline />
                    <App />
                </SnackbarProvider>
            </MuiPickersUtilsProvider>
        </MuiThemeProvider>
    </Provider>,
    document.getElementById('root')
);
