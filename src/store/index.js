import {applyMiddleware, createStore} from "redux";
import rootReducer from "./reducers/rootReducer";
import {createLogger} from "redux-logger";
import Thunk from "redux-thunk";

const configureStore = () => {

    const middleWares = [
        Thunk,
    ];

    const initialState = {};

    if (process.env.NODE_ENV !== 'production') {
        middleWares.push(createLogger({collapsed: true}));
    }

    return createStore(rootReducer, initialState, applyMiddleware(...middleWares));
};

export default configureStore();