import {DASHBOARD_CLEAR, DASHBOARD_SET, DASHBOARD_UPDATE_PROFILE_NAME} from "../actions/actionTypes";

const initialState = {
    profile: {},
    homePage: {},
    eventsPage: {},
    rentPage: {},
};

const dashboard = (state = initialState, action) => {
    switch (action.type) {
        case DASHBOARD_SET:
            return action.data;
        case DASHBOARD_UPDATE_PROFILE_NAME:
            return {
                ...state,
                profile: {
                    ...state.profile,
                    name: action.name,
                },
            };
        case DASHBOARD_CLEAR:
            return initialState;
        default:
            return state;
    }
};

export default dashboard;

export const getProfile = state => state.profile;
export const getHomePage = state => state.homePage;
export const getEventsPage = state => state.eventsPage;
export const getRentPage = state => state.rentPage;
export const getCompaniesPage = state => state.companiesPage;