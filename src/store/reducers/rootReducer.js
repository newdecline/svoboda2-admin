import {combineReducers} from "redux";
import authorization, * as fromAuthorization from "./authorization";
import prefetchCache, * as fromPrefetchCache from "./prefetchCache";
import dashboard, * as fromDashboard from "./dashboard";
import history, * as fromHistory from "./history";

const rootReducer = combineReducers({
    authorization,
    prefetchCache,
    dashboard,
    history,
});

export default rootReducer;

// Authorization
export const isAuthorizing = state => fromAuthorization.isAuthorizing(state.authorization);
export const isAuthorized = state => fromAuthorization.isAuthorized(state.authorization);
export const getAuthToken = state => fromAuthorization.getAuthToken(state.authorization);

// Dashboard
export const getProfile = state => fromDashboard.getProfile(state.dashboard);
export const getHomePage = state => fromDashboard.getHomePage(state.dashboard);
export const getEventsPage = state => fromDashboard.getEventsPage(state.dashboard);
export const getRentPage = state => fromDashboard.getRentPage(state.dashboard);
export const getCompaniesPage = state => fromDashboard.getCompaniesPage(state.dashboard);

// Prefetch Cache
export const getPrefetchCacheById = id => state => fromPrefetchCache.getPrefetchCacheById(state.prefetchCache, id);

// History
export const getFirstLoad = state => fromHistory.getFirstLoad(state.history);