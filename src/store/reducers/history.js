import {HISTORY_CHANGE_LOCATION} from "../actions/actionTypes";

const initialState = {
    locationChangeCounter: 0,
};

const history = (state = initialState, action) => {
    switch (action.type) {
        case HISTORY_CHANGE_LOCATION:
            return {
                ...state,
                locationChangeCounter: state.locationChangeCounter + 1,
            };
        default:
            return state;
    }
};

export default history;

export const getFirstLoad = state => state.locationChangeCounter <= 1;