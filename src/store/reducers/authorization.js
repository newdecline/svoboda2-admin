import {AUTHORIZATION_CLEAR, AUTHORIZATION_SET_AUTH_TOKEN, AUTHORIZATION_SET_IS_AUTHORIZED, AUTHORIZATION_SET_IS_AUTHORIZING} from "../actions/actionTypes";
import {getAuthToken as getAuthTokenFromLocalStorage} from "../../services/localStorage";

const authToken = getAuthTokenFromLocalStorage();

const initialState = {
    isAuthorizing: authToken !== null,
    isAuthorized: false,
    authToken: authToken,
};

const authorization = (state = initialState, action) => {
    switch (action.type) {
        case AUTHORIZATION_SET_IS_AUTHORIZED:
            return {
                ...state,
                isAuthorized: action.isAuthorized,
            };
        case AUTHORIZATION_SET_IS_AUTHORIZING:
            return {
                ...state,
                isAuthorizing: action.isAuthorizing,
            };
        case AUTHORIZATION_SET_AUTH_TOKEN:
            return {
                ...state,
                authToken: action.authToken,
            };
        case AUTHORIZATION_CLEAR:
            return {
                ...state,
                isAuthorized: false,
                authToken: null,
            };
        default:
            return state;
    }
};

export default authorization;

export const isAuthorizing = state => state.isAuthorizing;
export const isAuthorized = state => state.isAuthorized;
export const getAuthToken = state => state.authToken;