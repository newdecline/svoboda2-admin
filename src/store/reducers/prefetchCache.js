import {PREFETCH_CACHE_ADD} from "../actions/actionTypes";
import {combineReducers} from "redux";

const byId = (state ={}, action) => {
    switch (action.type) {
        case PREFETCH_CACHE_ADD:
            return {
                ...state,
                [action.id]: action.data,
            };
        default:
            return state;
    }
};

const prefetchCache = combineReducers({
    byId,
});

export default prefetchCache;

export const getPrefetchCacheById = (state, id) => state.byId[id];