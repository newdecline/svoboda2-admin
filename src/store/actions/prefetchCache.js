import {PREFETCH_CACHE_ADD} from "./actionTypes";

export const prefetchCacheAdd = (id, data) => ({
    type: PREFETCH_CACHE_ADD,
    id,
    data,
});