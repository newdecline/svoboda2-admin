import {AUTHORIZATION_CLEAR, AUTHORIZATION_SET_AUTH_TOKEN, AUTHORIZATION_SET_IS_AUTHORIZED, AUTHORIZATION_SET_IS_AUTHORIZING} from "./actionTypes";
import {logout as logoutRequest} from "../../api/http/authorization";
import {removeAuthToken, setAuthToken as saveAuthToken} from "../../services/localStorage";
import {getDashboard} from "../../api/http/dashboard";
import {clearDashboard, setDashboard} from "./dashboard";

export const setIsAuthorized = isAuthorized => ({
    type: AUTHORIZATION_SET_IS_AUTHORIZED,
    isAuthorized,
});

export const setIsAuthorizing = isAuthorizing => ({
    type: AUTHORIZATION_SET_IS_AUTHORIZING,
    isAuthorizing,
});

export const setAuthToken = authToken => ({
    type: AUTHORIZATION_SET_AUTH_TOKEN,
    authToken,
});

export const clear = () => ({
    type: AUTHORIZATION_CLEAR,
});

export const authorize = (makeRequest, authToken = null) => async dispatch => {
    dispatch(setIsAuthorizing(true));
    const response = await makeRequest(getDashboard(authToken));

    if (response.status === 200) {
        dispatch(setDashboard(response.data));
        dispatch(setIsAuthorized(true));
    }

    dispatch(setIsAuthorizing(false));
};

export const logout = makeRequest => async dispatch => {
    const response = await makeRequest(logoutRequest());

    if (response.status === 200 && response.data.success === true) {
        removeAuthToken();
        dispatch(clear());
        dispatch(clearDashboard());
    }
};

export const login = (token, makeRequest) => dispatch => {
    saveAuthToken(token);
    dispatch(setAuthToken(token));
    dispatch(authorize(makeRequest, token));
};