import {DASHBOARD_CLEAR, DASHBOARD_SET, DASHBOARD_UPDATE_PROFILE_NAME} from "./actionTypes";

export const setDashboard = data => ({
    type: DASHBOARD_SET,
    data,
});

export const updateProfileName = name => ({
    type: DASHBOARD_UPDATE_PROFILE_NAME,
    name,
});

export const clearDashboard = () => ({
    type: DASHBOARD_CLEAR,
});