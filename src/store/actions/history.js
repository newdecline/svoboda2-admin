import {HISTORY_CHANGE_LOCATION} from "./actionTypes";

export const changeLocation = () => ({
    type: HISTORY_CHANGE_LOCATION,
});